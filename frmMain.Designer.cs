﻿namespace carApp
{
    partial class frmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.汽车管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.新增车辆ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.所有汽车ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.事故违章ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.保养维修ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.车辆保险ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.年检ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.用车管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.申请用车ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.申请单ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.待还车ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.历史用车ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.其他信息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.加油ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.加油卡管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.充值管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.单位管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.系统管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.用户管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.修改密码ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.数据备份ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.接口管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.系统设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.日志查看ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.系统信息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.使用帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblyoukaJine0 = new System.Windows.Forms.LinkLabel();
            this.lblyoukaJine = new System.Windows.Forms.LinkLabel();
            this.lblyoukaCount = new System.Windows.Forms.LinkLabel();
            this.lblcarout = new System.Windows.Forms.LinkLabel();
            this.lblcarweixiu = new System.Windows.Forms.LinkLabel();
            this.lblcaruse = new System.Windows.Forms.LinkLabel();
            this.lblcartotal = new System.Windows.Forms.LinkLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.汽车管理ToolStripMenuItem,
            this.用车管理ToolStripMenuItem,
            this.其他信息ToolStripMenuItem,
            this.单位管理ToolStripMenuItem,
            this.系统管理ToolStripMenuItem,
            this.帮助ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 汽车管理ToolStripMenuItem
            // 
            this.汽车管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.新增车辆ToolStripMenuItem,
            this.所有汽车ToolStripMenuItem,
            this.事故违章ToolStripMenuItem,
            this.保养维修ToolStripMenuItem1,
            this.车辆保险ToolStripMenuItem,
            this.年检ToolStripMenuItem});
            this.汽车管理ToolStripMenuItem.Name = "汽车管理ToolStripMenuItem";
            this.汽车管理ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.汽车管理ToolStripMenuItem.Text = "汽车管理";
            // 
            // 新增车辆ToolStripMenuItem
            // 
            this.新增车辆ToolStripMenuItem.Name = "新增车辆ToolStripMenuItem";
            this.新增车辆ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.新增车辆ToolStripMenuItem.Text = "新增车辆";
            this.新增车辆ToolStripMenuItem.Click += new System.EventHandler(this.新增车辆ToolStripMenuItem_Click);
            // 
            // 所有汽车ToolStripMenuItem
            // 
            this.所有汽车ToolStripMenuItem.Name = "所有汽车ToolStripMenuItem";
            this.所有汽车ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.所有汽车ToolStripMenuItem.Text = "所有车辆";
            this.所有汽车ToolStripMenuItem.Click += new System.EventHandler(this.所有汽车ToolStripMenuItem_Click);
            // 
            // 事故违章ToolStripMenuItem
            // 
            this.事故违章ToolStripMenuItem.Name = "事故违章ToolStripMenuItem";
            this.事故违章ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.事故违章ToolStripMenuItem.Text = "事故违章";
            this.事故违章ToolStripMenuItem.Click += new System.EventHandler(this.事故违章ToolStripMenuItem_Click);
            // 
            // 保养维修ToolStripMenuItem1
            // 
            this.保养维修ToolStripMenuItem1.Name = "保养维修ToolStripMenuItem1";
            this.保养维修ToolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.保养维修ToolStripMenuItem1.Text = "保养维修";
            this.保养维修ToolStripMenuItem1.Click += new System.EventHandler(this.保养维修ToolStripMenuItem_Click);
            // 
            // 车辆保险ToolStripMenuItem
            // 
            this.车辆保险ToolStripMenuItem.Name = "车辆保险ToolStripMenuItem";
            this.车辆保险ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.车辆保险ToolStripMenuItem.Text = "保险";
            this.车辆保险ToolStripMenuItem.Click += new System.EventHandler(this.车辆保险ToolStripMenuItem_Click);
            // 
            // 年检ToolStripMenuItem
            // 
            this.年检ToolStripMenuItem.Name = "年检ToolStripMenuItem";
            this.年检ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.年检ToolStripMenuItem.Text = "年检";
            this.年检ToolStripMenuItem.Click += new System.EventHandler(this.年检ToolStripMenuItem_Click);
            // 
            // 用车管理ToolStripMenuItem
            // 
            this.用车管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.申请用车ToolStripMenuItem,
            this.申请单ToolStripMenuItem,
            this.待还车ToolStripMenuItem,
            this.历史用车ToolStripMenuItem});
            this.用车管理ToolStripMenuItem.Name = "用车管理ToolStripMenuItem";
            this.用车管理ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.用车管理ToolStripMenuItem.Text = "用车管理";
            // 
            // 申请用车ToolStripMenuItem
            // 
            this.申请用车ToolStripMenuItem.Name = "申请用车ToolStripMenuItem";
            this.申请用车ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.申请用车ToolStripMenuItem.Text = "申请用车";
            this.申请用车ToolStripMenuItem.Click += new System.EventHandler(this.申请用车ToolStripMenuItem_Click);
            // 
            // 申请单ToolStripMenuItem
            // 
            this.申请单ToolStripMenuItem.Name = "申请单ToolStripMenuItem";
            this.申请单ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.申请单ToolStripMenuItem.Text = "申请单";
            this.申请单ToolStripMenuItem.Click += new System.EventHandler(this.申请单ToolStripMenuItem_Click);
            // 
            // 待还车ToolStripMenuItem
            // 
            this.待还车ToolStripMenuItem.Name = "待还车ToolStripMenuItem";
            this.待还车ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.待还车ToolStripMenuItem.Text = "待还车";
            this.待还车ToolStripMenuItem.Click += new System.EventHandler(this.待还车ToolStripMenuItem_Click);
            // 
            // 历史用车ToolStripMenuItem
            // 
            this.历史用车ToolStripMenuItem.Name = "历史用车ToolStripMenuItem";
            this.历史用车ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.历史用车ToolStripMenuItem.Text = "历史用车";
            this.历史用车ToolStripMenuItem.Click += new System.EventHandler(this.历史用车ToolStripMenuItem_Click);
            // 
            // 其他信息ToolStripMenuItem
            // 
            this.其他信息ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.加油ToolStripMenuItem,
            this.加油卡管理ToolStripMenuItem,
            this.充值管理ToolStripMenuItem});
            this.其他信息ToolStripMenuItem.Name = "其他信息ToolStripMenuItem";
            this.其他信息ToolStripMenuItem.Size = new System.Drawing.Size(56, 21);
            this.其他信息ToolStripMenuItem.Text = "加油卡";
            // 
            // 加油ToolStripMenuItem
            // 
            this.加油ToolStripMenuItem.Name = "加油ToolStripMenuItem";
            this.加油ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.加油ToolStripMenuItem.Text = "加油登记";
            this.加油ToolStripMenuItem.Click += new System.EventHandler(this.加油ToolStripMenuItem_Click);
            // 
            // 加油卡管理ToolStripMenuItem
            // 
            this.加油卡管理ToolStripMenuItem.Name = "加油卡管理ToolStripMenuItem";
            this.加油卡管理ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.加油卡管理ToolStripMenuItem.Text = "加油卡登记";
            this.加油卡管理ToolStripMenuItem.Click += new System.EventHandler(this.加油卡管理ToolStripMenuItem_Click);
            // 
            // 充值管理ToolStripMenuItem
            // 
            this.充值管理ToolStripMenuItem.Name = "充值管理ToolStripMenuItem";
            this.充值管理ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.充值管理ToolStripMenuItem.Text = "加油卡充值";
            this.充值管理ToolStripMenuItem.Click += new System.EventHandler(this.充值管理ToolStripMenuItem_Click);
            // 
            // 单位管理ToolStripMenuItem
            // 
            this.单位管理ToolStripMenuItem.Name = "单位管理ToolStripMenuItem";
            this.单位管理ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.单位管理ToolStripMenuItem.Text = "单位管理";
            this.单位管理ToolStripMenuItem.Click += new System.EventHandler(this.单位管理ToolStripMenuItem_Click);
            // 
            // 系统管理ToolStripMenuItem
            // 
            this.系统管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.用户管理ToolStripMenuItem,
            this.修改密码ToolStripMenuItem,
            this.数据备份ToolStripMenuItem,
            this.接口管理ToolStripMenuItem,
            this.系统设置ToolStripMenuItem,
            this.日志查看ToolStripMenuItem});
            this.系统管理ToolStripMenuItem.Name = "系统管理ToolStripMenuItem";
            this.系统管理ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.系统管理ToolStripMenuItem.Text = "系统管理";
            // 
            // 用户管理ToolStripMenuItem
            // 
            this.用户管理ToolStripMenuItem.Name = "用户管理ToolStripMenuItem";
            this.用户管理ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.用户管理ToolStripMenuItem.Text = "用户管理";
            this.用户管理ToolStripMenuItem.Click += new System.EventHandler(this.用户管理ToolStripMenuItem_Click);
            // 
            // 修改密码ToolStripMenuItem
            // 
            this.修改密码ToolStripMenuItem.Name = "修改密码ToolStripMenuItem";
            this.修改密码ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.修改密码ToolStripMenuItem.Text = "修改密码";
            this.修改密码ToolStripMenuItem.Click += new System.EventHandler(this.修改密码ToolStripMenuItem_Click);
            // 
            // 数据备份ToolStripMenuItem
            // 
            this.数据备份ToolStripMenuItem.Name = "数据备份ToolStripMenuItem";
            this.数据备份ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.数据备份ToolStripMenuItem.Text = "数据备份";
            this.数据备份ToolStripMenuItem.Click += new System.EventHandler(this.数据备份ToolStripMenuItem_Click);
            // 
            // 接口管理ToolStripMenuItem
            // 
            this.接口管理ToolStripMenuItem.Name = "接口管理ToolStripMenuItem";
            this.接口管理ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.接口管理ToolStripMenuItem.Text = "接口管理";
            this.接口管理ToolStripMenuItem.Click += new System.EventHandler(this.接口管理ToolStripMenuItem_Click);
            // 
            // 系统设置ToolStripMenuItem
            // 
            this.系统设置ToolStripMenuItem.Name = "系统设置ToolStripMenuItem";
            this.系统设置ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.系统设置ToolStripMenuItem.Text = "系统设置";
            this.系统设置ToolStripMenuItem.Click += new System.EventHandler(this.系统设置ToolStripMenuItem_Click);
            // 
            // 日志查看ToolStripMenuItem
            // 
            this.日志查看ToolStripMenuItem.Name = "日志查看ToolStripMenuItem";
            this.日志查看ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.日志查看ToolStripMenuItem.Text = "日志查看";
            this.日志查看ToolStripMenuItem.Click += new System.EventHandler(this.日志查看ToolStripMenuItem_Click);
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.系统信息ToolStripMenuItem,
            this.使用帮助ToolStripMenuItem});
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.帮助ToolStripMenuItem.Text = "帮助";
            // 
            // 系统信息ToolStripMenuItem
            // 
            this.系统信息ToolStripMenuItem.Name = "系统信息ToolStripMenuItem";
            this.系统信息ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.系统信息ToolStripMenuItem.Text = "系统信息";
            this.系统信息ToolStripMenuItem.Click += new System.EventHandler(this.系统信息ToolStripMenuItem_Click);
            // 
            // 使用帮助ToolStripMenuItem
            // 
            this.使用帮助ToolStripMenuItem.Name = "使用帮助ToolStripMenuItem";
            this.使用帮助ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.使用帮助ToolStripMenuItem.Text = "使用帮助";
            this.使用帮助ToolStripMenuItem.Click += new System.EventHandler(this.使用帮助ToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel3,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 479);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(784, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(135, 17);
            this.toolStripStatusLabel1.Text = "技术支持QQ:22745529";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(601, 17);
            this.toolStripStatusLabel3.Spring = true;
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(33, 17);
            this.toolStripStatusLabel2.Text = "V1.0";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblyoukaJine0);
            this.groupBox1.Controls.Add(this.lblyoukaJine);
            this.groupBox1.Controls.Add(this.lblyoukaCount);
            this.groupBox1.Controls.Add(this.lblcarout);
            this.groupBox1.Controls.Add(this.lblcarweixiu);
            this.groupBox1.Controls.Add(this.lblcaruse);
            this.groupBox1.Controls.Add(this.lblcartotal);
            this.groupBox1.Location = new System.Drawing.Point(111, 191);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(563, 206);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // lblyoukaJine0
            // 
            this.lblyoukaJine0.AutoSize = true;
            this.lblyoukaJine0.Location = new System.Drawing.Point(374, 142);
            this.lblyoukaJine0.Name = "lblyoukaJine0";
            this.lblyoukaJine0.Size = new System.Drawing.Size(65, 12);
            this.lblyoukaJine0.TabIndex = 6;
            this.lblyoukaJine0.TabStop = true;
            this.lblyoukaJine0.Text = "无余额卡：";
            // 
            // lblyoukaJine
            // 
            this.lblyoukaJine.AutoSize = true;
            this.lblyoukaJine.Location = new System.Drawing.Point(237, 142);
            this.lblyoukaJine.Name = "lblyoukaJine";
            this.lblyoukaJine.Size = new System.Drawing.Size(77, 12);
            this.lblyoukaJine.TabIndex = 5;
            this.lblyoukaJine.TabStop = true;
            this.lblyoukaJine.Text = "可用总金额：";
            // 
            // lblyoukaCount
            // 
            this.lblyoukaCount.AutoSize = true;
            this.lblyoukaCount.Location = new System.Drawing.Point(103, 142);
            this.lblyoukaCount.Name = "lblyoukaCount";
            this.lblyoukaCount.Size = new System.Drawing.Size(65, 12);
            this.lblyoukaCount.TabIndex = 4;
            this.lblyoukaCount.TabStop = true;
            this.lblyoukaCount.Text = "加油卡数：";
            // 
            // lblcarout
            // 
            this.lblcarout.AutoSize = true;
            this.lblcarout.Location = new System.Drawing.Point(103, 87);
            this.lblcarout.Name = "lblcarout";
            this.lblcarout.Size = new System.Drawing.Size(53, 12);
            this.lblcarout.TabIndex = 3;
            this.lblcarout.TabStop = true;
            this.lblcarout.Text = "出勤中：";
            this.lblcarout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblcarout_LinkClicked);
            // 
            // lblcarweixiu
            // 
            this.lblcarweixiu.AutoSize = true;
            this.lblcarweixiu.Location = new System.Drawing.Point(374, 51);
            this.lblcarweixiu.Name = "lblcarweixiu";
            this.lblcarweixiu.Size = new System.Drawing.Size(77, 12);
            this.lblcarweixiu.TabIndex = 2;
            this.lblcarweixiu.TabStop = true;
            this.lblcarweixiu.Text = "维修保养中：";
            this.lblcarweixiu.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblcarweixiu_LinkClicked);
            // 
            // lblcaruse
            // 
            this.lblcaruse.AutoSize = true;
            this.lblcaruse.Location = new System.Drawing.Point(237, 51);
            this.lblcaruse.Name = "lblcaruse";
            this.lblcaruse.Size = new System.Drawing.Size(65, 12);
            this.lblcaruse.TabIndex = 1;
            this.lblcaruse.TabStop = true;
            this.lblcaruse.Text = "空闲车辆：";
            // 
            // lblcartotal
            // 
            this.lblcartotal.AutoSize = true;
            this.lblcartotal.Location = new System.Drawing.Point(103, 51);
            this.lblcartotal.Name = "lblcartotal";
            this.lblcartotal.Size = new System.Drawing.Size(65, 12);
            this.lblcartotal.TabIndex = 0;
            this.lblcartotal.TabStop = true;
            this.lblcartotal.Text = "车辆总数：";
            this.lblcartotal.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblcartotal_LinkClicked);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(138, 88);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(215, 51);
            this.button1.TabIndex = 0;
            this.button1.Text = "待审批用车单（0）";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(408, 88);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(233, 51);
            this.button2.TabIndex = 1;
            this.button2.Text = "未处理事故违章（0）";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(784, 501);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "汽车管理";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 汽车管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 新增车辆ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 用车管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 申请用车ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 申请单ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 历史用车ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 其他信息ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 加油ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 加油卡管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 单位管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 系统管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 用户管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 修改密码ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 数据备份ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 系统信息ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 所有汽车ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 接口管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 系统设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 日志查看ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 充值管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 保养维修ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 车辆保险ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 年检ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 事故违章ToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripMenuItem 使用帮助ToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.LinkLabel lblcartotal;
        private System.Windows.Forms.LinkLabel lblcaruse;
        private System.Windows.Forms.LinkLabel lblcarweixiu;
        private System.Windows.Forms.LinkLabel lblcarout;
        private System.Windows.Forms.LinkLabel lblyoukaCount;
        private System.Windows.Forms.LinkLabel lblyoukaJine;
        private System.Windows.Forms.LinkLabel lblyoukaJine0;
        private System.Windows.Forms.ToolStripMenuItem 待还车ToolStripMenuItem;
    }
}

