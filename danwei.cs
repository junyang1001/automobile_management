﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class danwei : Form
    {
        public danwei()
        {
            InitializeComponent();
        }
        int CurrentRowIndex = 0;
        int CurrentColumnIndex = 0;
        DataTable dt = new DataTable();
        void LoadData()
        {
            string sql = "select * from danwei";            
            dt = SqliteHelper.ExecuteTable(sql);
            dataGridView1.DataSource = dt;
        }
        void clearTxtVal()
        {
            txtmarks.Text = "";
            txtmingcheng.Text = "";
            txthaoma.Text = "";
            txtdizhi.Text = "";
            txtxingming.Text = "";
            lblId.Text = "0";
        }
        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            var dgv = (DataGridView)sender;
            CurrentRowIndex = e.RowIndex;
            CurrentColumnIndex = e.ColumnIndex;
        }
        bool openMouseRight = false;
        private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                openMouseRight = true;
                CurrentRowIndex = e.RowIndex;
                this.dataGridView1.Rows[e.RowIndex].Selected = true;
                this.dataGridView1.CurrentCell = this.dataGridView1.Rows[e.RowIndex].Cells[1];
                contextMenuStrip1.Show(Cursor.Position);
            }
            else
                openMouseRight = false;
        }

        private void 编辑ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            DataTable dt = db.GetTable("danwei", "id=" + id);
            Model.danwei mod = new Model.danwei();
            mod = db.FillModel<Model.danwei>(dt);
            if (mod != null)
            {
                txtdizhi.Text = mod.dizhi;
                txtmarks.Text = mod.marks;
                txthaoma.Text = mod.haoma;
                txtmingcheng.Text = mod.mingcheng;
                txtxingming.Text = mod.xingming;
                lblId.Text = mod.id.ToString();
                string typename = "公司类型";
              
                switch (mod.typeid)
                {
                    case 0:
                        typename = "汽车销售";
                        break;
                    case 1:
                        typename = "保养维修";
                        break;
                    case 2:
                        typename = "保险公司";
                        break;
                }
                comboBox1.Text = typename;
            }
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            deleteData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Model.danwei mod = new Model.danwei();
            mod.mingcheng = txtmingcheng.Text;
            mod.marks = txtmarks.Text;
            mod.dizhi = txtdizhi.Text;
            mod.haoma = txthaoma.Text;
            mod.xingming = txtxingming.Text;
            int typeid = 1;
            switch(comboBox1.Text)
            {
                case "汽车销售":
                    typeid = 0;
                    break;
                case "保养维修":
                    typeid = 1;
                    break;
                case "保险公司":
                    typeid = 2;
                    break;              
            }
            mod.typeid = typeid;
            if (lblId.Text == "0")
            {
                db.add(mod);
            }
            else
            {
                mod.id = int.Parse(lblId.Text);
                db.edit(mod);
            }
            clearTxtVal();
            LoadData();
        }

        private void danwei_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            LoadData();          
        }

        private void danwei_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.D && openMouseRight)
            {
                //deleteData();
                //contextMenuStrip1.Hide();
            }
        }
        void deleteData()
        {
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            SqliteHelper.ExecuteSql("delete from danwei where id=" + id);
            this.dataGridView1.Rows.RemoveAt(CurrentRowIndex);
        }
    }
}
