﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class v_log : Form
    {
        public v_log()
        {
            InitializeComponent();
        }

        private void v_log_Load(object sender, EventArgs e)
        {
            //var url = string.Format("https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={0}", Libs.wx.getToken());
            //StringBuilder sb = new StringBuilder();
            //sb.Append("{\"touser\":\"oZwlM1nwNfYDHJP6bvcRSkE5wExM\",");
            //sb.Append("\"template_id\":\"-_zTZ_qJnSSUPDto3O7MwwBDob_M2D-tCofE2fgC-zM\",");
            //sb.Append("\"miniprogram\":{\"appid\":\"wx296511084730b02f\"},\"data\":{");
            //sb.Append("\"first\":{\"value\":\"first\",\"color\":\"#173177\"},");
            //sb.Append("\"keyword1\":{\"value\":\"keyword1\",\"color\":\"#173177\"},");
            //sb.Append("\"keyword2\":{\"value\":\"keyword2\",\"color\":\"#173177\"},");
            //sb.Append("\"keyword3\":{\"value\":\"keyword3\",\"color\":\"#173177\"},");
            //sb.Append("\"remark\":{\"value\":\"remark\",\"color\":\"#173177\"}}}");
            //string result = "";
            //result = Libs.HttpHelper.HttpPost(url, sb.ToString());//HttpUtil.SendTemplateMsg(accessToken, postData);
            //textBox1.AppendText(result);
            if (Libs.FileHelper.IsExistFile("log.txt"))
            {
                StreamReader sr = new StreamReader("log.txt");
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    textBox1.AppendText(line+"\r\n");
                    textBox1.ScrollToCaret();
                }
                if (sr != null) sr.Close();

                //string str_line = "";
                //Action<String> dlg_log = delegate (string n) { textBox1.Text = n; };
                //this.textBox1.Invoke(dlg_log, new object[] { str_line });
            }
        }
    }
}
