﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class Config_sys : Form
    {
        public Config_sys()
        {
            InitializeComponent();
        }
        void setConfig(int id,int v)
        {
            SqliteHelper.ExecuteSql("update configs set val='" + v + "' where id="+id);
        }
        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            int v = checkBox4.Checked == true ? 1 : 0;
            setConfig(1, v);
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            int v = checkBox5.Checked == true ? 1 : 0;
            setConfig(2, v);
        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            int v = checkBox8.Checked == true ? 1 : 0;
            setConfig(3, v);
        }

        private void checkBox9_CheckedChanged(object sender, EventArgs e)
        {
            int v = checkBox9.Checked == true ? 1 : 0;
            setConfig(4, v);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            int v = checkBox1.Checked == true ? 1 : 0;
            setConfig(5, v);
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            int v = checkBox6.Checked == true ? 1 : 0;
            setConfig(6, v);
        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            int v = checkBox7.Checked == true ? 1 : 0;
            setConfig(7, v);
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            int v = checkBox2.Checked == true ? 1 : 0;
            setConfig(8, v);
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            int v = checkBox3.Checked == true ? 1 : 0;
            setConfig(9, v);
        }
    }
}
