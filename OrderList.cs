﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class OrderList : Form
    {
        public OrderList()
        {
            InitializeComponent();
        }
        DataTable dt = new DataTable();
        private void OrderList_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            LoadData();
            if(AppConfig.typeid !="管理员")
            {
                button1.Enabled = false;
                button2.Enabled = false;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in this.dataGridView1.SelectedRows)
            {
                if (!row.IsNewRow)
                {
                    int id = int.Parse(row.Cells[0].Value.ToString());
                    if(db.update("orders", "status=2", "id=" + id)>0)
                    {
                        if (db.count("configs", "id=7 and val=1") > 0)
                            db.WriteLog(6, AppConfig.username, "通过审批，"+row.Cells[0].Value.ToString() + "申请用车" + row.Cells[1].Value.ToString());
                    }
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if(dt.Rows[i]["id"].ToString()==id.ToString())
                        {
                            dt.Rows.RemoveAt(i);
                        }
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string s = Microsoft.VisualBasic.Interaction.InputBox("请输入说明", "驳回申请");
            foreach (DataGridViewRow row in this.dataGridView1.SelectedRows)
            {
                if (!row.IsNewRow)
                {
                    int id = int.Parse(row.Cells[0].Value.ToString());
                    if (db.update("orders", "status=0,remarks='" + s + "'", "id=" + id) > 0)
                        db.update("cars","status=1","chepai='"+ row.Cells[1].Value.ToString() + "'");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["id"].ToString() == id.ToString())
                        {
                            dt.Rows.RemoveAt(i);
                        }
                    }
                }
            }
        }
        #region pager
        int pageSize = 20;
        int CurrentPage = 1;
        int pageCount = 0;
        public void LoadData()
        {
            int RowCount = db.count("orders");           
            dt = db.GetPagerData("orders", "*", "status='1'",pageSize,CurrentPage);
            pageCount = (int)Math.Ceiling(RowCount * 1.0 / pageSize);
            lblTotalPage.Text = CurrentPage + "/"+pageCount+" 页";
            dataGridView1.DataSource = dt;
        }  
        
        private void label1_Click(object sender, EventArgs e)
        {
            CurrentPage = 1;
            LoadData();
        }
        #endregion

        private void label2_Click(object sender, EventArgs e)
        {
            CurrentPage += 1;
            if (CurrentPage > pageCount)
                CurrentPage = pageCount;
            LoadData();
        }

        private void lblprev_Click(object sender, EventArgs e)
        {
            CurrentPage -= 1;
            if (CurrentPage < 1)
                CurrentPage = 1;
            LoadData();
        }

        private void lbllast_Click(object sender, EventArgs e)
        {
            CurrentPage = pageCount;
            LoadData();
        }
    }
}
