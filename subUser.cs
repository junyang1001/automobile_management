﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class subUser : Form
    {
        public subUser()
        {
            InitializeComponent();
        }                
        int width = 0;
        private int CurrentRowIndex { get; set; }
        private int CurrentColumnIndex { get; set; }
        DataTable dt = new DataTable();
        private void subUser_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            bindGrid();
            for (int i = 1; i < this.dataGridView1.Columns.Count; i++)
            {
                this.dataGridView1.AutoResizeColumn(i, DataGridViewAutoSizeColumnMode.AllCells);
                width += this.dataGridView1.Columns[i].Width;
            }
            if (width > this.dataGridView1.Size.Width)
            {
                this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            }
            else
            {
                this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
            dataGridView1.Columns[0].Frozen = true;
        }
        void bindGrid()
        {
            dt = SqliteHelper.ExecuteTable("select * from users");
            dataGridView1.DataSource = dt;
        } 
        private void subUser_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 27)
            {
                this.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(txtxingming.TextLength>0 && txthaoma.TextLength>0)
            {
                string sql = "";
                string typeid = "操作员";
                if (AppConfig.userid == 1)
                    typeid = comboBox1.Text;
                if (lblId.Text=="0")
                {
                    sql = "insert into users(typeid,xingming,zhanghao,mima,haoma,danwei,bumen,zhiwu,jialing,jiashizheng) values(";
                    sql = sql + "'"+ typeid + "','" + txtxingming.Text + "','" + txtzhanghao.Text + "','" + Libs.Security.EncryptMD5(txtmima.Text) + "','" + txthaoma.Text + "','" + txtdanwei.Text + "'";
                    sql = sql + ",'" + txtbumen.Text + "','" + txtzhiwu.Text + "','" + txtjialing.Text + "','" + txtjiashizheng.Text + "')";
                }
                else
                {
                    sql = "update users set typeid='"+ typeid + "'";
                    sql=sql+",xingming='" + txtxingming.Text + "'";
                    sql=sql+",zhanghao='" + txtzhanghao.Text + "'";
                    if (txtmima.TextLength > 0)
                    {
                        sql = sql + ",mima='" + Libs.Security.EncryptMD5(txtmima.Text) + "'";
                    }
                    sql = sql + ",haoma='" + txthaoma.Text + "'" +
                    ",danwei='" + txtdanwei.Text + "'" +
                    ",bumen='" + txtbumen.Text + "'" +
                    ",zhiwu='" + txtzhiwu.Text + "'" +
                    ",jialing='" + txtjialing.Text + "'" +
                    ",jiashizheng = '" + txtjiashizheng.Text + "' where id=" + lblId.Text;
                    
                }
                SqliteHelper.ExecuteSql(sql);
                panel2.Visible = false;
            }
            bindGrid();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            panel2.Visible = true;
        }

        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            var dgv = (DataGridView)sender;
            CurrentRowIndex = e.RowIndex;
            CurrentColumnIndex = e.ColumnIndex;
        }

        private void contextMenuStrip1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)//判断是否当前弹起的右键
            {
                CurrentRowIndex = e.RowIndex;
                this.dataGridView1.Rows[e.RowIndex].Selected = true;
                this.dataGridView1.CurrentCell = this.dataGridView1.Rows[e.RowIndex].Cells[1];
                contextMenuStrip1.Show(Cursor.Position);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dt = SqliteHelper.ExecuteTable("select * from users where xingming='"+textBox1.Text+"'");
            dataGridView1.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            panel2.Visible = false;
        }

        private void 编辑ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            DataTable dt = db.GetTable("users", "id=" + id);
            Model.users mod = new Model.users();
            mod = db.FillModel<Model.users>(dt);
            if (mod != null)
            {
                txtbumen.Text = mod.bumen;
                txtdanwei.Text = mod.danwei;
                txthaoma.Text = mod.haoma;
                txtjialing.Text = mod.jialing;
                txtjiashizheng.Text = mod.jiashizheng;
                txtxingming.Text = mod.xingming;
                txtmima.Text = "";// mod.yue.ToString();
                txtzhanghao.Text = mod.zhanghao;
                txtzhiwu.Text = mod.zhiwu;
                lblId.Text = mod.id.ToString();
                panel2.Visible = true;
            }
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            SqliteHelper.ExecuteSql("delete from users where id=" + id);
            this.dataGridView1.Rows.RemoveAt(CurrentRowIndex);
        }
    }
}
