﻿using System;

namespace carApp.Model
{
    public class users
    {
        public int id { get; set; }
        public string zhanghao { get; set; }
        public string mima { get; set; }
        public string xingming { get; set; }
        public string haoma { get; set; }
        public string danwei { get; set; }
        public string bumen { get; set; }
        public string zhiwu { get; set; }
        public string jialing { get; set; }
        public string jiashizheng { get; set; }
    }
}
