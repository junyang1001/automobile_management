﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace carApp.Model
{
    public class orders
    {
        public int id { get; set; }
        public string xingming { get; set; }
        public string haoma { get; set; }
        public string chepai { get; set; }
        public string yongdt { get; set; }
        public string huandt { get; set; }
        public string marks { get; set; }
        public string mudi { get; set; }
        public int renshu { get; set; }
        public string shenpi { get; set; }
        public string remarks { get; set; }
        public int status { get; set; }
        public int isdel { get; set; }
    }
}

