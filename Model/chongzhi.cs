﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace carApp.Model
{
    public class chongzhi
    {
        public int id { get; set; }
        public string xingming { get; set; }
        public string kahao { get; set; }
        public string times { get; set; }
        public string marks { get; set; }
        public decimal jine { get; set; }
        public int typeid { get; set; }
    }
}
