﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace carApp.Model
{
    public class shigu
    {
        public int id { get; set; }
        public string chepai { get; set; }
        public string driver { get; set; }
        public string dizhi { get; set; }
        public string times { get; set; }
        public string xingming { get; set; }
        public string marks { get; set; }
        public string status { get; set; }
        public string shenpi { get; set; }
        public string remarks { get; set; }
    }
}
