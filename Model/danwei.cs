﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace carApp.Model
{
    public class danwei
    {
        public int id { get; set; }
        public int typeid { get; set; }
        public string mingcheng { get; set; }
        public string xingming { get; set; }
        public string haoma { get; set; }
        public string marks { get; set; }
        public string dizhi { get; set; }
    }
}
