﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace carApp.Model
{
    public class cars
    {
        public int id { get; set; }
        public string sn { get; set; }
        public string pinpai { get; set; }
        public string xinghao { get; set; }
        public string cheling { get; set; }
        public string pailiang { get; set; }
        public string ranyou { get; set; }
        public string zuowei { get; set; }
        public string yanse { get; set; }
        public string chezhong { get; set; }
        public string chepai { get; set; }
        public string xingzhi { get; set; }
        public string chezhu { get; set; }
        public string jiage { get; set; }
        public string maichedanwei { get; set; }
        public string maishijian { get; set; }
        public string fadongji { get; set; }
        public string chejia { get; set; }

    }
}
