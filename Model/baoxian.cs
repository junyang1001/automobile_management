﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace carApp.Model
{
    public class baoxian
    {
        public int id { get; set; }
        public string danwei { get; set; }
        public string chepai { get; set; }
        public string xingming { get; set; }
        public string times { get; set; }
        public string marks { get; set; }
        public string xianzhong { get; set; }
        public decimal jine { get; set; }
    }
}
