﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace carApp.Model
{
    public class logs
    {
        public int id { get; set; }
        public int typeid { get; set; }
        public string username { get; set; }
        public string contents { get; set; }
        public string times { get; set; }
    }
}
