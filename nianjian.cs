﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class nianjian : Form
    {
        public nianjian()
        {
            InitializeComponent();
        }
        int CurrentRowIndex = 0;
        int CurrentColumnIndex = 0;
        DataTable dt = new DataTable();
        void LoadData()
        {
            string sql = "select * from nianjian";          
            dt = SqliteHelper.ExecuteTable(sql);
            dataGridView1.DataSource = dt;
        }
        void clearTxtVal()
        {            
            txttimes.Text = "";
            txtxingming.Text = "";
            lblId.Text = "0";
        }
        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            var dgv = (DataGridView)sender;
            CurrentRowIndex = e.RowIndex;
            CurrentColumnIndex = e.ColumnIndex;
        }

        private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                CurrentRowIndex = e.RowIndex;
                this.dataGridView1.Rows[e.RowIndex].Selected = true;
                this.dataGridView1.CurrentCell = this.dataGridView1.Rows[e.RowIndex].Cells[1];
                contextMenuStrip1.Show(Cursor.Position);
            }
        }

        private void 编辑ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            DataTable dt = db.GetTable("nianjian", "id=" + id);
            Model.nianjian mod = new Model.nianjian();
            mod = db.FillModel<Model.nianjian>(dt);
            if (mod != null)
            {               
                txttimes.Text = mod.times;
                cmbchepai.Text = mod.chepai;
                txtxingming.Text = mod.xingming;
                cmbdanwei.Text = mod.danwei;
                lblId.Text = mod.id.ToString();
            }
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            SqliteHelper.ExecuteSql("delete from nianjian where id=" + id);
            this.dataGridView1.Rows.RemoveAt(CurrentRowIndex);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Model.nianjian mod = new Model.nianjian();          
            mod.times = txttimes.Text;
            mod.chepai = cmbchepai.Text;
            mod.xingming = txtxingming.Text;
            mod.danwei = cmbdanwei.Text;
            if (lblId.Text == "0")
            {
                db.add(mod);
            }
            else
            {
                mod.id = int.Parse(lblId.Text);
                db.edit(mod);
            }
            clearTxtVal();
            LoadData();
        }

        private void nianjian_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            LoadData();
            //cmbdanwei.DataSource = db.GetTable("danwei", "typeid=2", "id,mingcheng");
            //cmbdanwei.DisplayMember = "mingcheng";
            cmbchepai.DataSource = db.GetTable("cars", "", "id,chepai");
            cmbchepai.DisplayMember = "chepai";
        }
    }
}
