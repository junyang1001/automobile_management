﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class sysinfo : Form
    {
        public sysinfo()
        {
            InitializeComponent();
        }

        private void sysinfo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar==27)
            {
                this.Close();
            }
        }

        private void sysinfo_Load(object sender, EventArgs e)
        {
            string sysv = "";
            System.Net.WebClient myWebClient = new System.Net.WebClient();
            System.IO.Stream myStream = myWebClient.OpenRead("http://bbcweb.cn/car/getv.html");
            System.IO.StreamReader sr = new System.IO.StreamReader(myStream, System.Text.Encoding.GetEncoding("utf-8"));
            sysv = sr.ReadToEnd();
            myStream.Close();
            if(sysv!=lblver.Text)
            {
                lblnewv.Text = " 有新版本：" + sysv +" 可更新";
                lblnewv.Visible = true;
            }
        }
    }
}
