﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace carApp
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                string order1 = db.count("orders", "status=1").ToString();
                button1.Text = "待审批用车单（" + order1 + "）";

                string shigu1 = db.count("shigu", "status='未处理'").ToString();
                button2.Text = "未处理事故违章（" + shigu1 + "）";

                lblcarout.Text += db.count("orders", "status=2").ToString();
                lblcartotal.Text += db.count("cars", "").ToString();
                lblcaruse.Text += db.count("cars", "status=1").ToString();
                lblcarweixiu.Text += db.count("cars", "status=3").ToString();

                lblyoukaCount.Text += db.count("jiayouka", "").ToString() + "涨";
                lblyoukaJine0.Text += db.count("jiayouka", "yue=0").ToString() + "涨";
                lblyoukaJine.Text += db.sum("jiayouka", "yue", "").ToString() + "元";
            }
            catch(Exception ex)
            { throw ex; }
        }

        private void 用户管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            subUser frm = new subUser();
            frm.ShowDialog();
        }

        private void 新增车辆ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            carAdd frm = new carAdd();
            frm.ShowDialog();
        }

        private void 所有汽车ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            carlist frm = new carlist();
            frm.ShowDialog();
        }

        private void 申请用车ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OrderAdd frm = new OrderAdd();
            frm.ShowDialog();
        }

        private void 申请单ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OrderList frm = new OrderList();
            frm.ShowDialog();
        }

        private void 历史用车ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OrderLishi frm = new OrderLishi();
            frm.ShowDialog();
        }

        private void 加油卡管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Jiayouka frm = new Jiayouka();
            frm.ShowDialog();
        }

        private void 加油ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            jiayou frm = new jiayou();
            frm.ShowDialog();
        }

        private void 充值管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            jiayoukacz frm = new jiayoukacz();
            frm.ShowDialog();
        }


        private void 保养维修ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            baoyang frm = new baoyang();
            frm.ShowDialog();
        }

        private void 单位管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            danwei frm = new danwei();
            frm.ShowDialog();
        }

        private void 事故违章ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            shigu frm = new shigu();
            frm.ShowDialog();
        }

        private void 车辆保险ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            baoxian frm = new baoxian();
            frm.ShowDialog();
        }

        private void 年检ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            nianjian frm = new nianjian();
            frm.ShowDialog();
        }

        private void 修改密码ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string s = Microsoft.VisualBasic.Interaction.InputBox("请输入新密码", "修改密码");
            if (s.Length > 5)
            {
                db.update("users","mima='"+ Libs.Security.EncryptMD5(s) + "'","id="+AppConfig.userid);
            }
            else
            {
                MessageBox.Show("请设置6位数以上密码");
            }
        }

        private void 数据备份ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string newdbname = "car_bak_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".db";
            System.IO.File.Copy("car.db", newdbname);
            MessageBox.Show("已备份数据库文件："+ newdbname);
        }

        private void 接口管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Config_API frm = new Config_API();
            frm.ShowDialog();
        }

        private void 系统设置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Config_sys frm = new Config_sys();
            frm.ShowDialog();
        }

        private void 日志查看ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            v_log frm = new v_log();
            frm.ShowDialog();
        }

        private void 系统信息ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sysinfo frm = new sysinfo();
                frm.ShowDialog();
        }

        private void 使用帮助ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo.FileName = GetIERunString();
            string c = "http://bbcweb.cn/cars/help";
            p.StartInfo.Arguments = c;
            p.Start();
        }
        public static string GetIERunString()
        {
            string IEString = string.Empty;
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot;
            regKey = regKey.OpenSubKey(@"http\shell\open\command");
            IEString = regKey.GetValue("").ToString();
            string a = IEString;
            string[] b;
            b = a.Split(new char[1] { '"' });
            IEString = b[1];
            return IEString;
        }

        private void 待还车ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OrderHuanche frm = new OrderHuanche();
            frm.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OrderList frm = new OrderList();
            frm.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            shigu frm = new shigu();
            frm.ShowDialog();
        }

        private void lblcartotal_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            carlist frm = new carlist();
            frm.ShowDialog();
        }

        private void lblcarweixiu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            baoyang frm = new baoyang();
            frm.ShowDialog();
        }

        private void lblcarout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            OrderHuanche frm = new OrderHuanche();
            frm.ShowDialog();
        }
    }
}
