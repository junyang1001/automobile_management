﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class jiayou : Form
    {
        public jiayou()
        {
            InitializeComponent();
        }
        int CurrentRowIndex=0;
        int CurrentColumnIndex = 0;
        DataTable dt = new DataTable();
        private void button2_Click(object sender, EventArgs e)
        {
            LoadData();
        }
        void LoadData()
        {
            string sql = "select * from jiayou";
            if(txtkey.Text!= "车牌/姓名/加油卡号" && txtkey.TextLength>0)
            {
                sql = sql + " where (chepai='" + txtkey.Text + "' or xingming='" + txtkey.Text + "' or kahao='" + txtkey.Text+"')";
            }
            dt = SqliteHelper.ExecuteTable(sql);
            dataGridView1.DataSource = dt;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Model.jiayou mod = new Model.jiayou();
            mod.jine = decimal.Parse(txtjine.Text);
            mod.marks = txtmarks.Text;
            mod.times = txttimes.Text;
            mod.kahao = cmbkahao.Text;
            mod.xingming = txtxingming.Text;
            mod.chepai = cmbchepai.Text;
            if (lblId.Text == "0")
            {
                db.add(mod);
            }
            else
            {
                mod.id = int.Parse(lblId.Text);
                db.edit(mod);
            }
            clearTxtVal();
        }
        void clearTxtVal()
        {
            txtjine.Text = "";
            txtmarks.Text = "";
            txttimes.Text ="";
            cmbkahao.Text = "选择加油卡";
            txtxingming.Text = "";
            cmbchepai.Text = "选择车牌";
            lblId.Text = "0";
        }
        private void 编辑ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            DataTable dt = db.GetTable("jiayou", "id=" + id);
            Model.jiayou mod = new Model.jiayou();
            mod = db.FillModel<Model.jiayou>(dt);
            if (mod != null)
            {
                txtjine.Text = mod.jine.ToString();
                txtmarks.Text = mod.marks;
                txttimes.Text = mod.times;
                cmbkahao.Text = mod.kahao;
                txtxingming.Text = mod.xingming;
                cmbchepai.Text = mod.chepai;
                lblId.Text = mod.id.ToString();
            }
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            SqliteHelper.ExecuteSql("delete from jiayou where id=" + id);
            this.dataGridView1.Rows.RemoveAt(CurrentRowIndex);
        }

        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            var dgv = (DataGridView)sender;
            CurrentRowIndex = e.RowIndex;
            CurrentColumnIndex = e.ColumnIndex;
        }

        private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                CurrentRowIndex = e.RowIndex;
                this.dataGridView1.Rows[e.RowIndex].Selected = true;
                this.dataGridView1.CurrentCell = this.dataGridView1.Rows[e.RowIndex].Cells[1];
                contextMenuStrip1.Show(Cursor.Position);
            }
        }

        private void jiayou_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            LoadData();
            cmbchepai.DataSource = db.GetTable("cars", "", "id,chepai");
            cmbchepai.DisplayMember = "chepai";
            cmbkahao.DataSource = db.GetTable("jiayouka", "", "id,kahao");
            cmbkahao.DisplayMember = "kahao";
        }

        private void txtkey_Click(object sender, EventArgs e)
        {
            if (txtkey.Text == "车牌/姓名/加油卡号")
            {
                txtkey.Text = "";
            }
        }
    }
}
