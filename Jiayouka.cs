﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class Jiayouka : Form
    {
        public Jiayouka()
        {
            InitializeComponent();
        }
        DataTable dt = new DataTable();
        private void Jiayouka_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            LoadData();
        }
        void LoadData()
        {
            dt = SqliteHelper.ExecuteTable("select * from jiayouka");
            dataGridView1.DataSource = dt;
        }
        private int CurrentRowIndex { get; set; }
        private void contextMenuStrip1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            var dgv = (DataGridView)sender;
            CurrentRowIndex = e.RowIndex;
        }

        private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                CurrentRowIndex = e.RowIndex;
                this.dataGridView1.Rows[e.RowIndex].Selected = true;
                this.dataGridView1.CurrentCell = this.dataGridView1.Rows[e.RowIndex].Cells[1];
                contextMenuStrip1.Show(Cursor.Position);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Model.jiayouka mod = new Model.jiayouka();
            mod.chepai = txtchepai.Text;
            mod.danwei = txtdanwei.Text;
            mod.kahao = txtkahao.Text;
            mod.marks = txtmarks.Text;
            mod.times = txttimes.Text;
            mod.xingming = txtxingming.Text;
            mod.yue = decimal.Parse(txtyue.Text);
            mod.classname = cmbclassname.Text;
            mod.status = comboBox1.Text;
            mod.id = int.Parse(lblId.Text);
            if (lblId.Text == "0")
                db.add(mod);
            else
            {
                db.edit(mod);
            }
            LoadData();
            resetControl();
        }
        void resetControl()
        {
            Model.jiayouka mod = new Model.jiayouka();
            txtchepai.Text = mod.chepai;
            txtdanwei.Text = mod.danwei;
            txtkahao.Text = mod.kahao;
            txtmarks.Text = mod.marks;
            txttimes.Text = mod.times;
            txtxingming.Text = mod.xingming;
            txtyue.Text = mod.yue.ToString();
            cmbclassname.Text = mod.classname;
            comboBox1.Text = mod.status;
            lblId.Text = mod.id.ToString();
        }

        private void 编辑ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            DataTable dt = db.GetTable("jiayouka", "id=" + id);
            Model.jiayouka mod = new Model.jiayouka();
            mod = db.FillModel<Model.jiayouka>(dt);
            if (mod != null)
            {
                txtchepai.Text = mod.chepai;
                txtdanwei.Text = mod.danwei;
                txtkahao.Text = mod.kahao;
                txtmarks.Text = mod.marks;
                txttimes.Text = mod.times;
                txtxingming.Text = mod.xingming;
                txtyue.Text = mod.yue.ToString();
                cmbclassname.Text = mod.classname;
                comboBox1.Text = mod.status;
                lblId.Text = mod.id.ToString();
            }
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            SqliteHelper.ExecuteSql("delete from jiayouka where id=" + id);
            this.dataGridView1.Rows.RemoveAt(CurrentRowIndex);
        }
    }
}
