﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class baoxian : Form
    {
        public baoxian()
        {
            InitializeComponent();
        }
        int CurrentRowIndex = 0;
        int CurrentColumnIndex = 0;
        DataTable dt = new DataTable();
        void LoadData()
        {
            string sql = "select * from baoxian";
            if (txtkey.Text != "保险公司/车牌" && txtkey.TextLength > 0)
            {
                sql = sql + " where (chepai='" + txtkey.Text + "' or danwei='" + txtkey.Text + "')";
            }
            dt = SqliteHelper.ExecuteTable(sql);
            dataGridView1.DataSource = dt;
        }
        void clearTxtVal()
        {
            txtjine.Text = "0";
            txtmarks.Text = "";
            txttimes.Text = "";
            txtxingming.Text = "";
            lblId.Text = "0";
        }
        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            var dgv = (DataGridView)sender;
            CurrentRowIndex = e.RowIndex;
            CurrentColumnIndex = e.ColumnIndex;
        }

        private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                CurrentRowIndex = e.RowIndex;
                this.dataGridView1.Rows[e.RowIndex].Selected = true;
                this.dataGridView1.CurrentCell = this.dataGridView1.Rows[e.RowIndex].Cells[1];
                contextMenuStrip1.Show(Cursor.Position);
            }
        }

        private void 编辑ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            DataTable dt = db.GetTable("baoxian", "id=" + id);
            Model.baoxian mod = new Model.baoxian();
            mod = db.FillModel<Model.baoxian>(dt);
            if (mod != null)
            {
                txtjine.Text = mod.jine.ToString();
                txtmarks.Text = mod.marks;
                txttimes.Text = mod.times;
                cmbchepai.Text = mod.chepai;
                cmbxianzhong.Text = mod.xianzhong;
                txtxingming.Text = mod.xingming;
                cmbdanwei.Text = mod.danwei;
                lblId.Text = mod.id.ToString();
            }
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            SqliteHelper.ExecuteSql("delete from baoxian where id=" + id);
            this.dataGridView1.Rows.RemoveAt(CurrentRowIndex);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Model.baoxian mod = new Model.baoxian();
            mod.jine = decimal.Parse(txtjine.Text);
            mod.marks = txtmarks.Text;
            mod.times = txttimes.Text;
            mod.chepai = cmbchepai.Text;
            mod.xingming = txtxingming.Text;
            mod.xianzhong = cmbxianzhong.Text;
            mod.danwei = cmbdanwei.Text;
            if (lblId.Text == "0")
            {
                db.add(mod);
            }
            else
            {
                mod.id = int.Parse(lblId.Text);
                db.edit(mod);
            }
            clearTxtVal();
            LoadData();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void baoxian_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            LoadData();
            cmbdanwei.DataSource = db.GetTable("danwei", "typeid=2", "id,mingcheng");
            cmbdanwei.DisplayMember = "mingcheng";
            cmbchepai.DataSource = db.GetTable("cars", "", "id,chepai");
            cmbchepai.DisplayMember = "chepai";
        }

        private void txtkey_Click(object sender, EventArgs e)
        {
            if (txtkey.Text == "保险公司/车牌")
            {
                txtkey.Text = "";
            }
        }
    }
}
