﻿namespace carApp
{
    partial class carAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnsave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtsn = new System.Windows.Forms.TextBox();
            this.txtpinpai = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtxinghao = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtpailiang = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtzuowei = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtchezhong = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtxingzhi = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtchezhu = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtjiage = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtmaichedanwei = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtmaishijian = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtfadongji = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtchejiahao = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtyanse = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbranliao = new System.Windows.Forms.ComboBox();
            this.txtchepai = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtcheling = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnsave
            // 
            this.btnsave.Font = new System.Drawing.Font("宋体", 15F);
            this.btnsave.Location = new System.Drawing.Point(247, 363);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(85, 35);
            this.btnsave.TabIndex = 0;
            this.btnsave.Text = "保存";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 15F);
            this.label1.Location = new System.Drawing.Point(52, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "编号：";
            // 
            // txtsn
            // 
            this.txtsn.Font = new System.Drawing.Font("宋体", 15F);
            this.txtsn.Location = new System.Drawing.Point(127, 22);
            this.txtsn.Name = "txtsn";
            this.txtsn.Size = new System.Drawing.Size(205, 30);
            this.txtsn.TabIndex = 2;
            // 
            // txtpinpai
            // 
            this.txtpinpai.Font = new System.Drawing.Font("宋体", 15F);
            this.txtpinpai.Location = new System.Drawing.Point(127, 58);
            this.txtpinpai.Name = "txtpinpai";
            this.txtpinpai.Size = new System.Drawing.Size(205, 30);
            this.txtpinpai.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 15F);
            this.label2.Location = new System.Drawing.Point(52, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "品牌：";
            // 
            // txtxinghao
            // 
            this.txtxinghao.Font = new System.Drawing.Font("宋体", 15F);
            this.txtxinghao.Location = new System.Drawing.Point(127, 94);
            this.txtxinghao.Name = "txtxinghao";
            this.txtxinghao.Size = new System.Drawing.Size(205, 30);
            this.txtxinghao.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 15F);
            this.label3.Location = new System.Drawing.Point(52, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "型号：";
            // 
            // txtpailiang
            // 
            this.txtpailiang.Font = new System.Drawing.Font("宋体", 15F);
            this.txtpailiang.Location = new System.Drawing.Point(127, 130);
            this.txtpailiang.Name = "txtpailiang";
            this.txtpailiang.Size = new System.Drawing.Size(121, 30);
            this.txtpailiang.TabIndex = 8;
            this.txtpailiang.Text = "1.8L";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 15F);
            this.label4.Location = new System.Drawing.Point(52, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "排量：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 15F);
            this.label5.Location = new System.Drawing.Point(12, 169);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "燃料类型：";
            // 
            // txtzuowei
            // 
            this.txtzuowei.Font = new System.Drawing.Font("宋体", 15F);
            this.txtzuowei.Location = new System.Drawing.Point(127, 202);
            this.txtzuowei.Name = "txtzuowei";
            this.txtzuowei.Size = new System.Drawing.Size(121, 30);
            this.txtzuowei.TabIndex = 12;
            this.txtzuowei.Text = "4座";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 15F);
            this.label6.Location = new System.Drawing.Point(32, 206);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "座位数：";
            // 
            // txtchezhong
            // 
            this.txtchezhong.Font = new System.Drawing.Font("宋体", 15F);
            this.txtchezhong.Location = new System.Drawing.Point(127, 274);
            this.txtchezhong.Name = "txtchezhong";
            this.txtchezhong.Size = new System.Drawing.Size(121, 30);
            this.txtchezhong.TabIndex = 14;
            this.txtchezhong.Text = "0KG";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 15F);
            this.label7.Location = new System.Drawing.Point(52, 278);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 20);
            this.label7.TabIndex = 13;
            this.label7.Text = "车重：";
            // 
            // txtxingzhi
            // 
            this.txtxingzhi.Font = new System.Drawing.Font("宋体", 15F);
            this.txtxingzhi.Location = new System.Drawing.Point(471, 58);
            this.txtxingzhi.Name = "txtxingzhi";
            this.txtxingzhi.Size = new System.Drawing.Size(205, 30);
            this.txtxingzhi.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 15F);
            this.label8.Location = new System.Drawing.Point(357, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 20);
            this.label8.TabIndex = 15;
            this.label8.Text = "车辆性质：";
            // 
            // txtchezhu
            // 
            this.txtchezhu.Font = new System.Drawing.Font("宋体", 15F);
            this.txtchezhu.Location = new System.Drawing.Point(471, 94);
            this.txtchezhu.Name = "txtchezhu";
            this.txtchezhu.Size = new System.Drawing.Size(205, 30);
            this.txtchezhu.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 15F);
            this.label9.Location = new System.Drawing.Point(376, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 20);
            this.label9.TabIndex = 17;
            this.label9.Text = "所有人：";
            // 
            // txtjiage
            // 
            this.txtjiage.Font = new System.Drawing.Font("宋体", 15F);
            this.txtjiage.Location = new System.Drawing.Point(471, 130);
            this.txtjiage.Name = "txtjiage";
            this.txtjiage.Size = new System.Drawing.Size(205, 30);
            this.txtjiage.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 15F);
            this.label10.Location = new System.Drawing.Point(397, 134);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 20);
            this.label10.TabIndex = 19;
            this.label10.Text = "价格：";
            // 
            // txtmaichedanwei
            // 
            this.txtmaichedanwei.Font = new System.Drawing.Font("宋体", 15F);
            this.txtmaichedanwei.Location = new System.Drawing.Point(471, 166);
            this.txtmaichedanwei.Name = "txtmaichedanwei";
            this.txtmaichedanwei.Size = new System.Drawing.Size(205, 30);
            this.txtmaichedanwei.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 15F);
            this.label11.Location = new System.Drawing.Point(356, 170);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 20);
            this.label11.TabIndex = 21;
            this.label11.Text = "卖车单位：";
            // 
            // txtmaishijian
            // 
            this.txtmaishijian.Font = new System.Drawing.Font("宋体", 15F);
            this.txtmaishijian.Location = new System.Drawing.Point(471, 202);
            this.txtmaishijian.Name = "txtmaishijian";
            this.txtmaishijian.Size = new System.Drawing.Size(205, 30);
            this.txtmaishijian.TabIndex = 24;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 15F);
            this.label12.Location = new System.Drawing.Point(357, 206);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(109, 20);
            this.label12.TabIndex = 23;
            this.label12.Text = "购车时间：";
            // 
            // txtfadongji
            // 
            this.txtfadongji.Font = new System.Drawing.Font("宋体", 15F);
            this.txtfadongji.Location = new System.Drawing.Point(471, 275);
            this.txtfadongji.Name = "txtfadongji";
            this.txtfadongji.Size = new System.Drawing.Size(205, 30);
            this.txtfadongji.TabIndex = 26;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 15F);
            this.label13.Location = new System.Drawing.Point(356, 279);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(109, 20);
            this.label13.TabIndex = 25;
            this.label13.Text = "发动机号：";
            // 
            // txtchejiahao
            // 
            this.txtchejiahao.Font = new System.Drawing.Font("宋体", 15F);
            this.txtchejiahao.Location = new System.Drawing.Point(471, 311);
            this.txtchejiahao.Name = "txtchejiahao";
            this.txtchejiahao.Size = new System.Drawing.Size(205, 30);
            this.txtchejiahao.TabIndex = 28;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 15F);
            this.label14.Location = new System.Drawing.Point(376, 315);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(89, 20);
            this.label14.TabIndex = 27;
            this.label14.Text = "车架号：";
            // 
            // txtyanse
            // 
            this.txtyanse.Font = new System.Drawing.Font("宋体", 15F);
            this.txtyanse.Location = new System.Drawing.Point(127, 238);
            this.txtyanse.Name = "txtyanse";
            this.txtyanse.Size = new System.Drawing.Size(121, 30);
            this.txtyanse.TabIndex = 30;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 15F);
            this.label15.Location = new System.Drawing.Point(52, 242);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 20);
            this.label15.TabIndex = 29;
            this.label15.Text = "颜色：";
            // 
            // cmbranliao
            // 
            this.cmbranliao.Font = new System.Drawing.Font("宋体", 15F);
            this.cmbranliao.FormattingEnabled = true;
            this.cmbranliao.Items.AddRange(new object[] {
            "电动",
            "混合"});
            this.cmbranliao.Location = new System.Drawing.Point(127, 166);
            this.cmbranliao.Name = "cmbranliao";
            this.cmbranliao.Size = new System.Drawing.Size(121, 28);
            this.cmbranliao.TabIndex = 31;
            this.cmbranliao.Text = "燃油";
            // 
            // txtchepai
            // 
            this.txtchepai.Font = new System.Drawing.Font("宋体", 15F);
            this.txtchepai.Location = new System.Drawing.Point(471, 22);
            this.txtchepai.Name = "txtchepai";
            this.txtchepai.Size = new System.Drawing.Size(205, 30);
            this.txtchepai.TabIndex = 33;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 15F);
            this.label16.Location = new System.Drawing.Point(376, 25);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(89, 20);
            this.label16.TabIndex = 32;
            this.label16.Text = "车牌号：";
            // 
            // txtcheling
            // 
            this.txtcheling.Font = new System.Drawing.Font("宋体", 15F);
            this.txtcheling.Location = new System.Drawing.Point(471, 238);
            this.txtcheling.Name = "txtcheling";
            this.txtcheling.Size = new System.Drawing.Size(205, 30);
            this.txtcheling.TabIndex = 35;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 15F);
            this.label17.Location = new System.Drawing.Point(396, 242);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 20);
            this.label17.TabIndex = 34;
            this.label17.Text = "车龄：";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "yyyy-MM-dd hh:mm:ss";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(660, 206);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(16, 21);
            this.dateTimePicker1.TabIndex = 36;
            this.dateTimePicker1.CloseUp += new System.EventHandler(this.dateTimePicker1_CloseUp);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("宋体", 15F);
            this.button1.Location = new System.Drawing.Point(360, 363);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 35);
            this.button1.TabIndex = 37;
            this.button1.Text = "取消";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // carAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 427);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.txtcheling);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtchepai);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.cmbranliao);
            this.Controls.Add(this.txtyanse);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtchejiahao);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtfadongji);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtmaishijian);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtmaichedanwei);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtjiage);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtchezhu);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtxingzhi);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtchezhong);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtzuowei);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtpailiang);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtxinghao);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtpinpai);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtsn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnsave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "carAdd";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "添加车辆";
            this.Load += new System.EventHandler(this.carAdd_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtsn;
        private System.Windows.Forms.TextBox txtpinpai;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtxinghao;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtpailiang;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtzuowei;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtchezhong;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtxingzhi;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtchezhu;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtjiage;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtmaichedanwei;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtmaishijian;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtfadongji;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtchejiahao;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtyanse;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbranliao;
        private System.Windows.Forms.TextBox txtchepai;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtcheling;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button button1;
    }
}