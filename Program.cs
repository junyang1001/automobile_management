﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Login frm = new Login(); //登录 
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    Application.Run(new frmMain()); //主窗体 
                }
            }
            catch{}
            //db.CreateDatabase();
        }
    }
    public class AppConfig
    {
        public static string username { get; set; }
        public static int userid { get; set; }
        public static string typeid { get; set; }

    }
}
