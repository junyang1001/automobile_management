﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class carlist : Form
    {
        public carlist()
        {
            InitializeComponent();
        }
        DataTable dt = new DataTable();
        int CurrentRowIndex = 0;
        int CurrentColumnIndex = 0;
        private void carlist_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            LoadData();
        }

        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            var dgv = (DataGridView)sender;
            CurrentRowIndex = e.RowIndex;
            CurrentColumnIndex = e.ColumnIndex;
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            Model.cars mod = new Model.cars();
            mod.id = int.Parse(lblid.Text);
            mod.chejia = txtchejiahao.Text;
            mod.chepai = txtchepai.Text;
            mod.cheling = txtcheling.Text;
            mod.chezhong = txtchezhong.Text;
            mod.chezhu = txtchezhu.Text;
            mod.fadongji = txtfadongji.Text;
            mod.jiage = txtjiage.Text;
            mod.maichedanwei = txtmaichedanwei.Text;
            mod.maishijian = txtmaishijian.Text;
            mod.pailiang = txtpailiang.Text;
            mod.pinpai = txtpinpai.Text;
            mod.ranyou = cmbranliao.Text;
            mod.sn = txtsn.Text;
            mod.xinghao = txtxinghao.Text;
            mod.xingzhi = txtxingzhi.Text;
            mod.yanse = txtyanse.Text;
            mod.zuowei = txtzuowei.Text;
            db.edit(mod);
            panel1.Visible = false;
            LoadData();
        }

        private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)//判断是否当前弹起的右键
            {
                CurrentRowIndex = e.RowIndex;
                this.dataGridView1.Rows[e.RowIndex].Selected = true;//选中鼠标所在的当前行
                this.dataGridView1.CurrentCell = this.dataGridView1.Rows[e.RowIndex].Cells[1];//默认当前单元格为第一行第一个
                //this.contextMenuStrip1.Show(this.dataGridView1, e.Location);//右键菜单绑定当前位置，也就是第一行第一个
                contextMenuStrip1.Show(Cursor.Position);
            }
        }

        private void 编辑ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            DataTable dt = SqliteHelper.ExecuteTable("select * from cars where id=" + id);
            if (dt.Rows.Count > 0)
            {
                txtchejiahao.Text = dt.Rows[0]["chejia"].ToString();
                txtchepai.Text = dt.Rows[0]["chepai"].ToString();
                txtcheling.Text = dt.Rows[0]["cheling"].ToString();
                txtchezhong.Text = dt.Rows[0]["chezhong"].ToString();
                txtchezhu.Text = dt.Rows[0]["chezhu"].ToString();
                txtfadongji.Text = dt.Rows[0]["fadongji"].ToString();
                txtjiage.Text = dt.Rows[0]["jiage"].ToString();
                txtmaichedanwei.Text = dt.Rows[0]["maichedanwei"].ToString();
                txtmaishijian.Text = dt.Rows[0]["maishijian"].ToString();
                txtpailiang.Text = dt.Rows[0]["pailiang"].ToString();
                txtpinpai.Text = dt.Rows[0]["pinpai"].ToString();
                txtsn.Text = dt.Rows[0]["sn"].ToString();
                txtxinghao.Text = dt.Rows[0]["xinghao"].ToString();
                txtxingzhi.Text = dt.Rows[0]["xingzhi"].ToString();
                txtyanse.Text = dt.Rows[0]["yanse"].ToString();
                txtzuowei.Text = dt.Rows[0]["zuowei"].ToString();
                lblid.Text = dt.Rows[0]["id"].ToString();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
        }

        private void dateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            txtmaishijian.Text = dateTimePicker1.Value.ToShortDateString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtkey.Text != "车牌号")
                LoadData();
        }

        private void txtkey_Click(object sender, EventArgs e)
        {
            if (txtkey.Text == "车牌号")
                txtkey.Text = "";
        }
        #region pager
        int pageSize = 20;
        int CurrentPage = 1;
        int pageCount = 0;
        void LoadData()
        {
            string where = "";
            if (txtkey.Text != "车牌号" && txtkey.TextLength > 0)
                where = "chepai ='" + txtkey.Text + "'";
            int RowCount = db.count("cars");
            dt = db.GetPagerData("cars", "*", where, pageSize, CurrentPage);
            pageCount = (int)Math.Ceiling(RowCount * 1.0 / pageSize);
            lblTotalPage.Text = CurrentPage + "/" + pageCount + " 页";
            dataGridView1.DataSource = dt;
        }
        private void lblfirst_Click(object sender, EventArgs e)
        {
            CurrentPage = 1;
            LoadData();
        }

        private void lblprev_Click(object sender, EventArgs e)
        {
            CurrentPage -= 1;
            if (CurrentPage < 1)
                CurrentPage = 1;
        }

        private void lblNext_Click(object sender, EventArgs e)
        {
            CurrentPage += 1;
            if (CurrentPage > pageCount)
                CurrentPage = pageCount;
            LoadData();
        }

        private void lbllast_Click(object sender, EventArgs e)
        {
            CurrentPage = pageCount;
        }
        #endregion

        private void txtkey_Leave(object sender, EventArgs e)
        {
            if (txtkey.TextLength == 0)
                txtkey.Text = "车牌号";
        }

        private void txtkey_TextChanged(object sender, EventArgs e)
        {

        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            SqliteHelper.ExecuteSql("delete from cars where id=" + id);
            this.dataGridView1.Rows.RemoveAt(CurrentRowIndex);
        }
    }
}
