﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class OrderLishi : Form
    {
        public OrderLishi()
        {
            InitializeComponent();
        }
        DataTable dt = new DataTable();
        private void OrderLishi_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            LoadData();
        }
        #region pager
        int pageSize = 20;
        int CurrentPage = 1;
        int pageCount = 0;
        int RowCount = 0;
        void LoadData()
        {
            string where = "";
            if (txtkey.Text != "车牌号/姓名" && txtkey.TextLength > 0)
                where = "(chepai ='" + txtkey.Text + "' or xingming='"+txtkey.Text+"')";
            if(RowCount==0)
                RowCount = db.count("orders");
            dt = db.GetPagerData("orders", "*", where, pageSize, CurrentPage,"status asc");           
            pageCount = (int)Math.Ceiling(RowCount * 1.0 / pageSize);
            lblTotalPage.Text = CurrentPage + "/" + pageCount + " 页";
            dataGridView1.DataSource = dt;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                string st = dataGridView1.Rows[i].Cells[7].Value.ToString();
                switch (st)
                {
                    case "0":
                        st = "已取消,"+dt.Rows[i]["remarks"].ToString();
                        break;
                    case "1":
                        st = "待审";
                        break;
                    case "2":
                        st = "已审批";
                        break;
                    case "3":
                        st = "待还车";
                        break;
                    case "5":
                        st = "已还车";
                        break;
                }
                Type t = dataGridView1.Rows[i].Cells[7].Value.GetType();
                dataGridView1.Rows[i].Cells[7].Value = st;
            }
        }
        private void lblfirst_Click(object sender, EventArgs e)
        {
            CurrentPage = 1;
            LoadData();
        }

        private void lblprev_Click(object sender, EventArgs e)
        {
            CurrentPage -= 1;
            if (CurrentPage < 1)
                CurrentPage = 1;
        }

        private void lblNext_Click(object sender, EventArgs e)
        {
            CurrentPage += 1;
            if (CurrentPage > pageCount)
                CurrentPage = pageCount;
            LoadData();
        }

        private void lbllast_Click(object sender, EventArgs e)
        {
            CurrentPage = pageCount;
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void txtkey_Click(object sender, EventArgs e)
        {
            txtkey.Text = "";
        }       
        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
           
        }
    }
}
