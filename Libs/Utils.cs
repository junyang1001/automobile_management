﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace carApp.Libs
{
    public class Utils
    {
        public static bool IsIP(string ip)
        {
            return Regex.IsMatch(ip, @"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$");
        }
        public static string getIP()
        {
            string hostName = Dns.GetHostName();
            //IPAddress[] iplist = Dns.GetHostAddresses(hostName);
            //foreach(IPAddress ip in iplist)
            IPHostEntry iplist = Dns.GetHostByName(hostName);
            return iplist.AddressList[0].ToString();
        }
        public static string getDateTime()
        {
            return DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
        }
        public static int StrDateDiffSeconds(string time, int Sec)
        {
            DateTime dateTime = Convert.ToDateTime(time);
            if (dateTime.ToString("yyyy-MM-dd") == "1900-01-01")
                return 1;
            TimeSpan ts = DateTime.Now - dateTime.AddSeconds(Sec);
            if (ts.TotalSeconds > int.MaxValue)
                return int.MaxValue;
            else if (ts.TotalSeconds < int.MinValue)
                return int.MinValue;
            return (int)ts.TotalSeconds;
        }
        public static bool isSaveCashie(string str)
        {
            if (IsSafeSqlString(str))
                return true;
            return false;
        }
        /// <summary>
        /// 检测是否有Sql危险字符
        /// </summary>
        /// <param name="str">要判断字符串</param>
        /// <returns>判断结果</returns>
        public static bool IsSafeSqlString(string str)
        {
            return !Regex.IsMatch(str, @"[-|;|,|\/|\(|\)|\[|\]|\}|\{|%|@|\*|!|\'|=]");
        }
        /// <summary>
        /// 改正sql语句中的转义字符
        /// </summary>
        public static string mashSQL(string str)
        {
            string str2;
            if (str == null)
            {
                str2 = "";
            }
            else
            {
                str = str.Replace("\'", "'");
                str2 = str;
            }
            return str2;
        }
        public static bool isNumber(string str)
        {
            return Regex.IsMatch(str,@"^[0-9]\d*$");
        }
        public static bool isNumber(string value,bool isPoint)
        {
            return Regex.IsMatch(value, @"^[+-]?\d*[.]?\d*$");
        }
        public static bool IsInt(string value)
        {
            return Regex.IsMatch(value, @"^[+-]?\d*$");
        }
        public static bool IsUnsign(string value)
        {
            return Regex.IsMatch(value, @"^\d*[.]?\d*$");
        }

        public static bool isTel(string strInput)
        {
            return Regex.IsMatch(strInput, @"\d{3}-\d{8}|\d{4}-\d{7}");
        }
    }
}
