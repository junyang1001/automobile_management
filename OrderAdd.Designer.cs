﻿namespace carApp
{
    partial class OrderAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtxingming = new System.Windows.Forms.TextBox();
            this.txthaoma = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbchepai = new System.Windows.Forms.ComboBox();
            this.txtmudi = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtyongdt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txthuandt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtmarks = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.txtrenshu = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 15F);
            this.label1.Location = new System.Drawing.Point(55, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "申请人：";
            // 
            // txtxingming
            // 
            this.txtxingming.Font = new System.Drawing.Font("宋体", 15F);
            this.txtxingming.Location = new System.Drawing.Point(133, 30);
            this.txtxingming.Name = "txtxingming";
            this.txtxingming.Size = new System.Drawing.Size(171, 30);
            this.txtxingming.TabIndex = 1;
            // 
            // txthaoma
            // 
            this.txthaoma.Font = new System.Drawing.Font("宋体", 15F);
            this.txthaoma.Location = new System.Drawing.Point(421, 30);
            this.txthaoma.Name = "txthaoma";
            this.txthaoma.Size = new System.Drawing.Size(171, 30);
            this.txthaoma.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 15F);
            this.label2.Location = new System.Drawing.Point(363, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "电话：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 15F);
            this.label3.Location = new System.Drawing.Point(75, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "车辆：";
            // 
            // cmbchepai
            // 
            this.cmbchepai.Font = new System.Drawing.Font("宋体", 15F);
            this.cmbchepai.FormattingEnabled = true;
            this.cmbchepai.Location = new System.Drawing.Point(133, 89);
            this.cmbchepai.Name = "cmbchepai";
            this.cmbchepai.Size = new System.Drawing.Size(171, 28);
            this.cmbchepai.TabIndex = 3;
            // 
            // txtmudi
            // 
            this.txtmudi.Font = new System.Drawing.Font("宋体", 15F);
            this.txtmudi.Location = new System.Drawing.Point(133, 145);
            this.txtmudi.Name = "txtmudi";
            this.txtmudi.Size = new System.Drawing.Size(171, 30);
            this.txtmudi.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 15F);
            this.label4.Location = new System.Drawing.Point(55, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "目的地：";
            // 
            // txtyongdt
            // 
            this.txtyongdt.Font = new System.Drawing.Font("宋体", 15F);
            this.txtyongdt.Location = new System.Drawing.Point(421, 88);
            this.txtyongdt.Name = "txtyongdt";
            this.txtyongdt.Size = new System.Drawing.Size(153, 30);
            this.txtyongdt.TabIndex = 5;
            this.txtyongdt.EnabledChanged += new System.EventHandler(this.txtyongdt_EnabledChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 15F);
            this.label5.Location = new System.Drawing.Point(323, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "用车时间：";
            // 
            // txthuandt
            // 
            this.txthuandt.Font = new System.Drawing.Font("宋体", 15F);
            this.txthuandt.Location = new System.Drawing.Point(421, 145);
            this.txthuandt.Name = "txthuandt";
            this.txthuandt.Size = new System.Drawing.Size(153, 30);
            this.txthuandt.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 15F);
            this.label6.Location = new System.Drawing.Point(323, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 20);
            this.label6.TabIndex = 10;
            this.label6.Text = "还车时间：";
            // 
            // txtmarks
            // 
            this.txtmarks.Font = new System.Drawing.Font("宋体", 15F);
            this.txtmarks.Location = new System.Drawing.Point(133, 247);
            this.txtmarks.Multiline = true;
            this.txtmarks.Name = "txtmarks";
            this.txtmarks.Size = new System.Drawing.Size(459, 69);
            this.txtmarks.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 15F);
            this.label7.Location = new System.Drawing.Point(75, 250);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 20);
            this.label7.TabIndex = 12;
            this.label7.Text = "用途：";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("宋体", 15F);
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(133, 342);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 38);
            this.button1.TabIndex = 8;
            this.button1.Text = "提交申请";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DarkRed;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("宋体", 15F);
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Location = new System.Drawing.Point(246, 342);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(90, 38);
            this.button2.TabIndex = 10;
            this.button2.Text = "关闭";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(116, 270);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(11, 12);
            this.label8.TabIndex = 16;
            this.label8.Text = "0";
            this.label8.Visible = false;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("宋体", 15F);
            this.dateTimePicker1.Location = new System.Drawing.Point(572, 88);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(20, 30);
            this.dateTimePicker1.TabIndex = 17;
            this.dateTimePicker1.CloseUp += new System.EventHandler(this.dateTimePicker1_CloseUp);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Font = new System.Drawing.Font("宋体", 15F);
            this.dateTimePicker2.Location = new System.Drawing.Point(572, 145);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(20, 30);
            this.dateTimePicker2.TabIndex = 18;
            this.dateTimePicker2.CloseUp += new System.EventHandler(this.dateTimePicker2_CloseUp);
            // 
            // txtrenshu
            // 
            this.txtrenshu.Font = new System.Drawing.Font("宋体", 15F);
            this.txtrenshu.Location = new System.Drawing.Point(133, 196);
            this.txtrenshu.Name = "txtrenshu";
            this.txtrenshu.Size = new System.Drawing.Size(171, 30);
            this.txtrenshu.TabIndex = 19;
            this.txtrenshu.Text = "1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 15F);
            this.label9.Location = new System.Drawing.Point(75, 199);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 20);
            this.label9.TabIndex = 20;
            this.label9.Text = "人数：";
            // 
            // OrderAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 419);
            this.Controls.Add(this.txtrenshu);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtmarks);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txthuandt);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtyongdt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtmudi);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbchepai);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txthaoma);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtxingming);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "OrderAdd";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = ".  天涯60";
            this.Load += new System.EventHandler(this.OrderAdd_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtxingming;
        private System.Windows.Forms.TextBox txthaoma;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbchepai;
        private System.Windows.Forms.TextBox txtmudi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtyongdt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txthuandt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtmarks;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.TextBox txtrenshu;
        private System.Windows.Forms.Label label9;
    }
}