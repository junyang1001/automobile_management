﻿namespace carApp
{
    partial class carlist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblTotalPage = new System.Windows.Forms.Label();
            this.lbllast = new System.Windows.Forms.Label();
            this.lblNext = new System.Windows.Forms.Label();
            this.lblprev = new System.Windows.Forms.Label();
            this.lblfirst = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtkey = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button2 = new System.Windows.Forms.Button();
            this.txtcheling = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lblid = new System.Windows.Forms.Label();
            this.txtchepai = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbranliao = new System.Windows.Forms.ComboBox();
            this.txtyanse = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtchejiahao = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtfadongji = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtmaishijian = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtmaichedanwei = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtjiage = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtchezhu = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtxingzhi = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtchezhong = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtzuowei = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtpailiang = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtxinghao = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtpinpai = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtsn = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnsave = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.编辑ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.txtkey);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(800, 505);
            this.panel2.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblTotalPage);
            this.panel3.Controls.Add(this.lbllast);
            this.panel3.Controls.Add(this.lblNext);
            this.panel3.Controls.Add(this.lblprev);
            this.panel3.Controls.Add(this.lblfirst);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(235, 473);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(565, 32);
            this.panel3.TabIndex = 5;
            // 
            // lblTotalPage
            // 
            this.lblTotalPage.AutoSize = true;
            this.lblTotalPage.Location = new System.Drawing.Point(341, 10);
            this.lblTotalPage.Name = "lblTotalPage";
            this.lblTotalPage.Size = new System.Drawing.Size(47, 12);
            this.lblTotalPage.TabIndex = 9;
            this.lblTotalPage.Text = "共 0 页";
            // 
            // lbllast
            // 
            this.lbllast.AutoSize = true;
            this.lbllast.Location = new System.Drawing.Point(306, 10);
            this.lbllast.Name = "lbllast";
            this.lbllast.Size = new System.Drawing.Size(29, 12);
            this.lbllast.TabIndex = 8;
            this.lbllast.Text = "末页";
            this.lbllast.Click += new System.EventHandler(this.lbllast_Click);
            // 
            // lblNext
            // 
            this.lblNext.AutoSize = true;
            this.lblNext.Location = new System.Drawing.Point(259, 10);
            this.lblNext.Name = "lblNext";
            this.lblNext.Size = new System.Drawing.Size(41, 12);
            this.lblNext.TabIndex = 7;
            this.lblNext.Text = "下一页";
            this.lblNext.Click += new System.EventHandler(this.lblNext_Click);
            // 
            // lblprev
            // 
            this.lblprev.AutoSize = true;
            this.lblprev.Location = new System.Drawing.Point(212, 10);
            this.lblprev.Name = "lblprev";
            this.lblprev.Size = new System.Drawing.Size(41, 12);
            this.lblprev.TabIndex = 6;
            this.lblprev.Text = "上一页";
            this.lblprev.Click += new System.EventHandler(this.lblprev_Click);
            // 
            // lblfirst
            // 
            this.lblfirst.AutoSize = true;
            this.lblfirst.Location = new System.Drawing.Point(177, 10);
            this.lblfirst.Name = "lblfirst";
            this.lblfirst.Size = new System.Drawing.Size(29, 12);
            this.lblfirst.TabIndex = 5;
            this.lblfirst.Text = "首页";
            this.lblfirst.Click += new System.EventHandler(this.lblfirst_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(173, 478);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(56, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "搜索";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtkey
            // 
            this.txtkey.Location = new System.Drawing.Point(5, 479);
            this.txtkey.Name = "txtkey";
            this.txtkey.Size = new System.Drawing.Size(165, 21);
            this.txtkey.TabIndex = 2;
            this.txtkey.Text = "车牌号";
            this.txtkey.Click += new System.EventHandler(this.txtkey_Click);
            this.txtkey.TextChanged += new System.EventHandler(this.txtkey_TextChanged);
            this.txtkey.Leave += new System.EventHandler(this.txtkey_Leave);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.txtcheling);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.lblid);
            this.panel1.Controls.Add(this.txtchepai);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.cmbranliao);
            this.panel1.Controls.Add(this.txtyanse);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.txtchejiahao);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.txtfadongji);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.txtmaishijian);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.txtmaichedanwei);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.txtjiage);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.txtchezhu);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.txtxingzhi);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtchezhong);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtzuowei);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtpailiang);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtxinghao);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtpinpai);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtsn);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnsave);
            this.panel1.Location = new System.Drawing.Point(33, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(738, 411);
            this.panel1.TabIndex = 4;
            this.panel1.Visible = false;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(679, 206);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(20, 21);
            this.dateTimePicker1.TabIndex = 71;
            this.dateTimePicker1.CloseUp += new System.EventHandler(this.dateTimePicker1_CloseUp);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("宋体", 15F);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(384, 355);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(85, 38);
            this.button2.TabIndex = 70;
            this.button2.Text = "关闭";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtcheling
            // 
            this.txtcheling.Font = new System.Drawing.Font("宋体", 15F);
            this.txtcheling.Location = new System.Drawing.Point(494, 238);
            this.txtcheling.Name = "txtcheling";
            this.txtcheling.Size = new System.Drawing.Size(205, 30);
            this.txtcheling.TabIndex = 69;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 15F);
            this.label17.Location = new System.Drawing.Point(419, 242);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 20);
            this.label17.TabIndex = 68;
            this.label17.Text = "车龄：";
            // 
            // lblid
            // 
            this.lblid.AutoSize = true;
            this.lblid.Location = new System.Drawing.Point(344, 317);
            this.lblid.Name = "lblid";
            this.lblid.Size = new System.Drawing.Size(11, 12);
            this.lblid.TabIndex = 67;
            this.lblid.Text = "0";
            this.lblid.Visible = false;
            // 
            // txtchepai
            // 
            this.txtchepai.Font = new System.Drawing.Font("宋体", 15F);
            this.txtchepai.Location = new System.Drawing.Point(494, 21);
            this.txtchepai.Name = "txtchepai";
            this.txtchepai.Size = new System.Drawing.Size(205, 30);
            this.txtchepai.TabIndex = 66;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 15F);
            this.label16.Location = new System.Drawing.Point(399, 24);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(89, 20);
            this.label16.TabIndex = 65;
            this.label16.Text = "车牌号：";
            // 
            // cmbranliao
            // 
            this.cmbranliao.Font = new System.Drawing.Font("宋体", 15F);
            this.cmbranliao.FormattingEnabled = true;
            this.cmbranliao.Items.AddRange(new object[] {
            "电动",
            "混合"});
            this.cmbranliao.Location = new System.Drawing.Point(150, 165);
            this.cmbranliao.Name = "cmbranliao";
            this.cmbranliao.Size = new System.Drawing.Size(121, 28);
            this.cmbranliao.TabIndex = 64;
            this.cmbranliao.Text = "燃油";
            // 
            // txtyanse
            // 
            this.txtyanse.Font = new System.Drawing.Font("宋体", 15F);
            this.txtyanse.Location = new System.Drawing.Point(150, 237);
            this.txtyanse.Name = "txtyanse";
            this.txtyanse.Size = new System.Drawing.Size(121, 30);
            this.txtyanse.TabIndex = 63;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 15F);
            this.label15.Location = new System.Drawing.Point(75, 241);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 20);
            this.label15.TabIndex = 62;
            this.label15.Text = "颜色：";
            // 
            // txtchejiahao
            // 
            this.txtchejiahao.Font = new System.Drawing.Font("宋体", 15F);
            this.txtchejiahao.Location = new System.Drawing.Point(494, 310);
            this.txtchejiahao.Name = "txtchejiahao";
            this.txtchejiahao.Size = new System.Drawing.Size(205, 30);
            this.txtchejiahao.TabIndex = 61;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 15F);
            this.label14.Location = new System.Drawing.Point(399, 314);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(89, 20);
            this.label14.TabIndex = 60;
            this.label14.Text = "车架号：";
            // 
            // txtfadongji
            // 
            this.txtfadongji.Font = new System.Drawing.Font("宋体", 15F);
            this.txtfadongji.Location = new System.Drawing.Point(494, 274);
            this.txtfadongji.Name = "txtfadongji";
            this.txtfadongji.Size = new System.Drawing.Size(205, 30);
            this.txtfadongji.TabIndex = 59;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 15F);
            this.label13.Location = new System.Drawing.Point(379, 278);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(109, 20);
            this.label13.TabIndex = 58;
            this.label13.Text = "发动机号：";
            // 
            // txtmaishijian
            // 
            this.txtmaishijian.Font = new System.Drawing.Font("宋体", 15F);
            this.txtmaishijian.Location = new System.Drawing.Point(494, 201);
            this.txtmaishijian.Name = "txtmaishijian";
            this.txtmaishijian.Size = new System.Drawing.Size(205, 30);
            this.txtmaishijian.TabIndex = 57;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 15F);
            this.label12.Location = new System.Drawing.Point(380, 205);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(109, 20);
            this.label12.TabIndex = 56;
            this.label12.Text = "购车时间：";
            // 
            // txtmaichedanwei
            // 
            this.txtmaichedanwei.Font = new System.Drawing.Font("宋体", 15F);
            this.txtmaichedanwei.Location = new System.Drawing.Point(494, 165);
            this.txtmaichedanwei.Name = "txtmaichedanwei";
            this.txtmaichedanwei.Size = new System.Drawing.Size(205, 30);
            this.txtmaichedanwei.TabIndex = 55;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 15F);
            this.label11.Location = new System.Drawing.Point(379, 169);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 20);
            this.label11.TabIndex = 54;
            this.label11.Text = "卖车单位：";
            // 
            // txtjiage
            // 
            this.txtjiage.Font = new System.Drawing.Font("宋体", 15F);
            this.txtjiage.Location = new System.Drawing.Point(494, 129);
            this.txtjiage.Name = "txtjiage";
            this.txtjiage.Size = new System.Drawing.Size(205, 30);
            this.txtjiage.TabIndex = 53;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 15F);
            this.label10.Location = new System.Drawing.Point(420, 133);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 20);
            this.label10.TabIndex = 52;
            this.label10.Text = "价格：";
            // 
            // txtchezhu
            // 
            this.txtchezhu.Font = new System.Drawing.Font("宋体", 15F);
            this.txtchezhu.Location = new System.Drawing.Point(494, 93);
            this.txtchezhu.Name = "txtchezhu";
            this.txtchezhu.Size = new System.Drawing.Size(205, 30);
            this.txtchezhu.TabIndex = 51;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 15F);
            this.label9.Location = new System.Drawing.Point(399, 97);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 20);
            this.label9.TabIndex = 50;
            this.label9.Text = "所有人：";
            // 
            // txtxingzhi
            // 
            this.txtxingzhi.Font = new System.Drawing.Font("宋体", 15F);
            this.txtxingzhi.Location = new System.Drawing.Point(494, 57);
            this.txtxingzhi.Name = "txtxingzhi";
            this.txtxingzhi.Size = new System.Drawing.Size(205, 30);
            this.txtxingzhi.TabIndex = 49;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 15F);
            this.label8.Location = new System.Drawing.Point(380, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 20);
            this.label8.TabIndex = 48;
            this.label8.Text = "车辆性质：";
            // 
            // txtchezhong
            // 
            this.txtchezhong.Font = new System.Drawing.Font("宋体", 15F);
            this.txtchezhong.Location = new System.Drawing.Point(150, 273);
            this.txtchezhong.Name = "txtchezhong";
            this.txtchezhong.Size = new System.Drawing.Size(121, 30);
            this.txtchezhong.TabIndex = 47;
            this.txtchezhong.Text = "0KG";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 15F);
            this.label7.Location = new System.Drawing.Point(75, 277);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 20);
            this.label7.TabIndex = 46;
            this.label7.Text = "车重：";
            // 
            // txtzuowei
            // 
            this.txtzuowei.Font = new System.Drawing.Font("宋体", 15F);
            this.txtzuowei.Location = new System.Drawing.Point(150, 201);
            this.txtzuowei.Name = "txtzuowei";
            this.txtzuowei.Size = new System.Drawing.Size(121, 30);
            this.txtzuowei.TabIndex = 45;
            this.txtzuowei.Text = "4座";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 15F);
            this.label6.Location = new System.Drawing.Point(55, 205);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 20);
            this.label6.TabIndex = 44;
            this.label6.Text = "座位数：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 15F);
            this.label5.Location = new System.Drawing.Point(35, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 20);
            this.label5.TabIndex = 43;
            this.label5.Text = "燃料类型：";
            // 
            // txtpailiang
            // 
            this.txtpailiang.Font = new System.Drawing.Font("宋体", 15F);
            this.txtpailiang.Location = new System.Drawing.Point(150, 129);
            this.txtpailiang.Name = "txtpailiang";
            this.txtpailiang.Size = new System.Drawing.Size(121, 30);
            this.txtpailiang.TabIndex = 42;
            this.txtpailiang.Text = "1.8L";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 15F);
            this.label4.Location = new System.Drawing.Point(75, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 20);
            this.label4.TabIndex = 41;
            this.label4.Text = "排量：";
            // 
            // txtxinghao
            // 
            this.txtxinghao.Font = new System.Drawing.Font("宋体", 15F);
            this.txtxinghao.Location = new System.Drawing.Point(150, 93);
            this.txtxinghao.Name = "txtxinghao";
            this.txtxinghao.Size = new System.Drawing.Size(205, 30);
            this.txtxinghao.TabIndex = 40;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 15F);
            this.label3.Location = new System.Drawing.Point(75, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 20);
            this.label3.TabIndex = 39;
            this.label3.Text = "型号：";
            // 
            // txtpinpai
            // 
            this.txtpinpai.Font = new System.Drawing.Font("宋体", 15F);
            this.txtpinpai.Location = new System.Drawing.Point(150, 57);
            this.txtpinpai.Name = "txtpinpai";
            this.txtpinpai.Size = new System.Drawing.Size(205, 30);
            this.txtpinpai.TabIndex = 38;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 15F);
            this.label2.Location = new System.Drawing.Point(75, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 20);
            this.label2.TabIndex = 37;
            this.label2.Text = "品牌：";
            // 
            // txtsn
            // 
            this.txtsn.Font = new System.Drawing.Font("宋体", 15F);
            this.txtsn.Location = new System.Drawing.Point(150, 21);
            this.txtsn.Name = "txtsn";
            this.txtsn.Size = new System.Drawing.Size(205, 30);
            this.txtsn.TabIndex = 36;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 15F);
            this.label1.Location = new System.Drawing.Point(75, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 20);
            this.label1.TabIndex = 35;
            this.label1.Text = "编号：";
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnsave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsave.Font = new System.Drawing.Font("宋体", 15F);
            this.btnsave.ForeColor = System.Drawing.Color.White;
            this.btnsave.Location = new System.Drawing.Point(293, 355);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(85, 38);
            this.btnsave.TabIndex = 34;
            this.btnsave.Text = "保存";
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(800, 473);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellMouseEnter);
            this.dataGridView1.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseUp);
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "id";
            this.Column7.HeaderText = "";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Visible = false;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "chepai";
            this.Column1.HeaderText = "车牌号";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "pinpai";
            this.Column2.HeaderText = "品牌";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "xinghao";
            this.Column3.HeaderText = "车型";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "zuowei";
            this.Column4.HeaderText = "座位";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "ranyou";
            this.Column5.HeaderText = "燃料";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "cheling";
            this.Column6.HeaderText = "车龄";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.编辑ToolStripMenuItem,
            this.删除ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(181, 70);
            // 
            // 编辑ToolStripMenuItem
            // 
            this.编辑ToolStripMenuItem.Name = "编辑ToolStripMenuItem";
            this.编辑ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.编辑ToolStripMenuItem.Text = "编辑";
            this.编辑ToolStripMenuItem.Click += new System.EventHandler(this.编辑ToolStripMenuItem_Click);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.删除ToolStripMenuItem.Text = "删除";
            this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
            // 
            // carlist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 505);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "carlist";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "车辆列表";
            this.Load += new System.EventHandler(this.carlist_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtkey;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 编辑ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtchepai;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbranliao;
        private System.Windows.Forms.TextBox txtyanse;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtchejiahao;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtfadongji;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtmaishijian;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtmaichedanwei;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtjiage;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtchezhu;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtxingzhi;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtchezhong;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtzuowei;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtpailiang;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtxinghao;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtpinpai;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtsn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Label lblid;
        private System.Windows.Forms.TextBox txtcheling;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblTotalPage;
        private System.Windows.Forms.Label lbllast;
        private System.Windows.Forms.Label lblNext;
        private System.Windows.Forms.Label lblprev;
        private System.Windows.Forms.Label lblfirst;
    }
}