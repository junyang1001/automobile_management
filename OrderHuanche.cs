﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class OrderHuanche : Form
    {
        public OrderHuanche()
        {
            InitializeComponent();
        }

        private void OrderHuanche_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            LoadData();
        }
        DataTable dt = new DataTable();
        void LoadData()
        {
            string where = "status=2";         
            int RowCount = db.count("orders", where);
            dt = db.GetPagerData("orders", "*", where, 1000, 1);            
            dataGridView1.DataSource = dt;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                string st = dataGridView1.Rows[i].Cells[7].Value.ToString();
                switch (st)
                {
                    case "0":
                        st = "已取消," + dt.Rows[i]["remarks"].ToString();
                        break;
                    case "1":
                        st = "待审";
                        break;
                    case "2":
                        st = "已审批";
                        break;
                    case "3":
                        st = "待还车";
                        break;
                    case "5":
                        st = "已还车";
                        break;
                }
                Type t = dataGridView1.Rows[i].Cells[7].Value.GetType();
                dataGridView1.Rows[i].Cells[7].Value = st;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in this.dataGridView1.SelectedRows)
            {
                if (!row.IsNewRow)
                {
                    int id = int.Parse(row.Cells[0].Value.ToString());
                    if (db.update("orders", "status=5", "id=" + id) > 0)
                        db.update("cars", "status=1", "chepai='" + row.Cells[1].Value.ToString() + "'");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["id"].ToString() == id.ToString())
                        {
                            dt.Rows.RemoveAt(i);
                        }
                    }
                }
            }
        }
    }
}
