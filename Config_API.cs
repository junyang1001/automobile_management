﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class Config_API : Form
    {
        public Config_API()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string sql = "update configapi set APIUrl='',username='" + appid.Text + "'";
            sql = sql + ",passw='" + appsecrety.Text + "',marks='',status='' where typeid=2";
            SqliteHelper.ExecuteSql(sql);
            MessageBox.Show("已保存");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sql = "update configapi set APIUrl='" + smsurl.Text + "',username='" + smsuser.Text + "'";
            sql=sql+ ",passw='" + smspassw.Text + "',marks='" + smstemplate.Text + "',status='" + checkBox1.Checked.ToString() + "' where typeid=1";
            SqliteHelper.ExecuteSql(sql);
            MessageBox.Show("已保存");
        }

        private void Config_API_Load(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = SqliteHelper.ExecuteTable("select * from configapi where typeid=1");
                smspassw.Text = dt.Rows[0]["passw"].ToString();
                smstemplate.Text = dt.Rows[0]["marks"].ToString();
                smsurl.Text = dt.Rows[0]["apiurl"].ToString();
                smsuser.Text = dt.Rows[0]["username"].ToString();
                if (dt.Rows[0]["status"].ToString() == "True")
                    checkBox1.Checked = true;
                else
                    checkBox1.Checked = false;
                DataTable dtwx = SqliteHelper.ExecuteTable("select * from configapi where typeid=2");
                appid.Text = dtwx.Rows[0]["username"].ToString();
                appsecrety.Text = dtwx.Rows[0]["passw"].ToString();
            }
            catch
            { }
        }
    }
}
