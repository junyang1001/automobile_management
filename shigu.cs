﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class shigu : Form
    {
        public shigu()
        {
            InitializeComponent();
        }
        int CurrentRowIndex = 0;
        int CurrentColumnIndex = 0;
        DataTable dt = new DataTable();
        void LoadData()
        {
            string sql = "select * from shigu";
            //if (txtkey.Text != "姓名/车牌" && txtkey.TextLength > 0)
            //{
            //    sql = sql + " where (chepai='" + txtkey.Text + "' or driver='" + txtkey.Text + "')";
            //}
            dt = SqliteHelper.ExecuteTable(sql);
            dataGridView1.DataSource = dt;
        }
        void clearTxtVal()
        {
            txtdizhi.Text = "";
            txtmarks.Text = "";
            txttimes.Text = "";
            txtdriver.Text = "";
            txtxingming.Text = "";
            txtremarks.Text = "";
            txtshenpi.Text = "";            
            lblId.Text = "0";
        }
        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            var dgv = (DataGridView)sender;
            CurrentRowIndex = e.RowIndex;
            CurrentColumnIndex = e.ColumnIndex;
        }

        private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                CurrentRowIndex = e.RowIndex;
                this.dataGridView1.Rows[e.RowIndex].Selected = true;
                this.dataGridView1.CurrentCell = this.dataGridView1.Rows[e.RowIndex].Cells[1];
                contextMenuStrip1.Show(Cursor.Position);
            }
        }

        private void 编辑ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            DataTable dt = db.GetTable("shigu", "id=" + id);
            Model.shigu mod = new Model.shigu();
            mod = db.FillModel<Model.shigu>(dt);
            if (mod != null)
            {
                txtdriver.Text = mod.driver;
                txtmarks.Text = mod.marks;
                txttimes.Text = mod.times;
                txtremarks.Text = mod.remarks;
                txtxingming.Text = mod.xingming;
                txtshenpi.Text = mod.shenpi;
                cmbchepai.Text = mod.chepai;
                cmbstatus.Text = mod.status;
                txtdizhi.Text = mod.dizhi;
                lblId.Text = mod.id.ToString();
            }
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            SqliteHelper.ExecuteSql("delete from shigu where id=" + id);
            this.dataGridView1.Rows.RemoveAt(CurrentRowIndex);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Model.shigu mod = new Model.shigu();
            mod.driver = txtdriver.Text;
            mod.marks = txtmarks.Text;
            mod.times = txttimes.Text;
            mod.chepai = cmbchepai.Text;
            mod.xingming = txtxingming.Text;
            mod.status = cmbstatus.Text;
            mod.shenpi = txtshenpi.Text;
            mod.remarks = txtremarks.Text;
            mod.dizhi = txtdizhi.Text;          
            if (lblId.Text == "0")
            {
                db.add(mod);
            }
            else
            {
                mod.id = int.Parse(lblId.Text);
                db.edit(mod);
            }
            clearTxtVal();
            LoadData();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void shigu_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            LoadData();           
            cmbchepai.DataSource = db.GetTable("cars", "", "id,chepai");
            cmbchepai.DisplayMember = "chepai";
        }

    }
}
