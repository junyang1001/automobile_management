﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class OrderAdd : Form
    {
        public OrderAdd()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(txtyongdt.TextLength==0 || txthuandt.TextLength==0)
            {
                MessageBox.Show("请填写用车日期和还车日期");
                return;
            }
            Model.orders mod = new Model.orders();
            mod.chepai = cmbchepai.Text;
            mod.haoma = txthaoma.Text;
            mod.huandt = txthuandt.Text;
            mod.marks = txtmarks.Text;
            mod.mudi = txtmudi.Text;
            mod.shenpi = "";
            mod.remarks = "";
            mod.status = 1;
            mod.renshu = int.Parse(txtrenshu.Text);
            mod.xingming = txtxingming.Text;
            mod.yongdt = txtyongdt.Text;
            if (db.add(mod) > 0)
            {
                db.update("cars","status=2","chepai='"+cmbchepai.Text+"'");
                if (db.count("configs", "id=6 and val=1") > 0)
                    db.WriteLog(6, AppConfig.username, txtxingming.Text+"申请用车"+cmbchepai.Text);
                MessageBox.Show("提交申请成功");
            }
            else
                MessageBox.Show("失败");
            this.Close();

        }

        private void OrderAdd_Load(object sender, EventArgs e)
        {
            cmbchepai.DisplayMember = "chepai";
            cmbchepai.DataSource = db.GetTable("cars","status=1","id,chepai");
        }

        private void txtyongdt_EnabledChanged(object sender, EventArgs e)
        {
            
        }

        private void dateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            txtyongdt.Text = dateTimePicker1.Value.ToString();
        }

        private void dateTimePicker2_CloseUp(object sender, EventArgs e)
        {
            txthuandt.Text = dateTimePicker2.Value.ToString();
        }
    }
}
