﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class baoyang : Form
    {
        public baoyang()
        {
            InitializeComponent();
        }
        int CurrentRowIndex = 0;
        int CurrentColumnIndex = 0;
        DataTable dt = new DataTable();
        void LoadData()
        {
            string sql = "select * from baoyang";
            if (txtkey.Text != "姓名/车牌" && txtkey.TextLength > 0)
            {
                sql = sql + " where (chepai='" + txtkey.Text + "' or xingming='" + txtkey.Text + "')";
            }
            dt = SqliteHelper.ExecuteTable(sql);
            dataGridView1.DataSource = dt;
        }
        void clearTxtVal()
        {
            txtjine.Text = "0";
            txtmarks.Text = "";
            txttimes.Text = "";
            cmbtypeid.Text = "保养";
            txtxingming.Text = "";
            lblId.Text = "0";
        }
        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            var dgv = (DataGridView)sender;
            CurrentRowIndex = e.RowIndex;
            CurrentColumnIndex = e.ColumnIndex;
        }

        private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                CurrentRowIndex = e.RowIndex;
                this.dataGridView1.Rows[e.RowIndex].Selected = true;
                this.dataGridView1.CurrentCell = this.dataGridView1.Rows[e.RowIndex].Cells[1];
                contextMenuStrip1.Show(Cursor.Position);
            }
        }

        private void 编辑ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            DataTable dt = db.GetTable("baoyang", "id=" + id);
            Model.baoyang mod = new Model.baoyang();
            mod = db.FillModel<Model.baoyang>(dt);
            if (mod != null)
            {
                txtjine.Text = mod.jine.ToString();
                txtmarks.Text = mod.marks;
                txttimes.Text = mod.times;
                cmbtypeid.Text = mod.typeid;
                txtxingming.Text = mod.xingming;
                txtfap.Text = mod.fap;                
                lblId.Text = mod.id.ToString();
            }
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.Rows[CurrentRowIndex].Cells[0].Value;
            SqliteHelper.ExecuteSql("delete from baoyang where id=" + id);
            this.dataGridView1.Rows.RemoveAt(CurrentRowIndex);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Model.baoyang mod = new Model.baoyang();
            mod.jine = decimal.Parse(txtjine.Text);
            mod.marks = txtmarks.Text;
            mod.times = txttimes.Text;
            mod.chepai = cmbchepai.Text;
            mod.xingming = txtxingming.Text;
            mod.typeid = cmbtypeid.Text;
            mod.danwei = cmbdanwei.Text;
            mod.fap = txtfap.Text;            
            if (lblId.Text == "0")
            {
                db.add(mod);
            }
            else
            {
                mod.id = int.Parse(lblId.Text);
                db.edit(mod);
            }
            clearTxtVal();
            LoadData();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void baoyang_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            LoadData();
            cmbdanwei.DataSource = db.GetTable("danwei", "", "id,mingcheng");
            cmbdanwei.DisplayMember = "mingcheng";
            cmbchepai.DataSource = db.GetTable("cars", "", "id,chepai");
            cmbchepai.DisplayMember = "chepai";
        }

        private void txtkey_Click(object sender, EventArgs e)
        {
            if (txtkey.Text == "姓名/车牌")
            {
                txtkey.Text = "";
            }
        }
    }
}
