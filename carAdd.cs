﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace carApp
{
    public partial class carAdd : Form
    {
        public carAdd()
        {
            InitializeComponent();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if(txtsn.TextLength>0)
            {
                Model.cars mod = new Model.cars();
                mod.chejia = txtchejiahao.Text;
                mod.chepai = txtchepai.Text;
                mod.chezhong = txtchezhong.Text;
                mod.chezhu = txtchezhu.Text;
                mod.fadongji = txtfadongji.Text;
                mod.jiage = txtjiage.Text;
                mod.maichedanwei = txtmaichedanwei.Text;
                mod.maishijian = txtmaishijian.Text;
                mod.pailiang = txtpailiang.Text;
                mod.pinpai = txtpinpai.Text;
                mod.ranyou = cmbranliao.Text;
                mod.sn = txtsn.Text;
                mod.xinghao = txtxinghao.Text;
                mod.xingzhi = txtxingzhi.Text;
                mod.yanse = txtyanse.Text;
                mod.zuowei = txtzuowei.Text;
                mod.cheling = txtcheling.Text;
                db.add(mod);
            }
        }

        private void carAdd_Load(object sender, EventArgs e)
        {
            string maxid = SqliteHelper.ExecuteScalar("select max(id) from cars");
            if (maxid == "")
                maxid = "1";
            else
                maxid = (int.Parse(maxid)+1).ToString();
            txtsn.Text = maxid.PadLeft(5, '0');
        }

        private void dateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            txtmaishijian.Text = dateTimePicker1.Value.ToShortDateString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
